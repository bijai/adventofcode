from argparse import *

def findMovementCost(p_dict,value):
    movement = 0
    for val in p_dict:
        if(val != value):
            movement+=(abs(value-val) * p_dict[val])
    return movement


def mainLogic(data):
    crabs = [int(x) for x in data[0].split(',')]
    crabDict = {}
    maxCrab = 0
    maxValCrab = 0
    maxVal = 0
    for crab in crabs:
        if( crab in crabDict):
            crabDict[crab]+=1
            if(maxVal < crabDict[crab]):
                maxVal = crabDict[crab]
                maxValCrab = crab
        else :
            crabDict[crab] = 1
            if(crab > maxCrab):
                maxCrab=crab
    
    print(maxCrab,maxValCrab,maxVal)
    movement = findMovementCost(crabDict,maxValCrab)
    i = 1
    while True:
        prevMovement = movement
        movement = findMovementCost(crabDict,maxValCrab+i)
        print(maxValCrab+i,movement)
        if(prevMovement < movement):
            break
        i+=1





def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
