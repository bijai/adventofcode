from argparse import *

LITERAL = 4

LENTYPE_TOTAL = 0
LENTYPE_COUNT = 1


class Packet:
    def __init__(self, packetString) -> None:
        self.packet = packetString
        self.version = -1
        self.remainder = ""
        self.val = -1
        self.subLenCount = -1
        self.subPackets = []
        self.lenType = -1
        self.packetType = -1
        self.decodePacket()

    def decodePacket(self):
        packet = self.packet
        self.version = int(packet[0:3], 2)
        self.packetType = int(packet[3:6], 2)

        if(self.packetType != LITERAL):
            self.parseOperatorPacket()
        else:
            self.parseLiteralPacket()

    def parseOperatorPacket(self):
        self.lenType = int(self.packet[6], 2)
        if(self.lenType == LENTYPE_TOTAL):
            subPacketlength = int(self.packet[7:7+15], 2)
            self.subLenCount = subPacketlength
            subPacketString = self.packet[7+15:7+15+subPacketlength]
            self.remainder = self.packet[7+15+subPacketlength:]
            print(str(self))
            while len(subPacketString) > 0:
                subPacket = Packet(subPacketString)
                self.subPackets.append(subPacket)
                subPacketString = subPacket.remainder
            
        elif(self.lenType == LENTYPE_COUNT):
            subPacketCount = int(self.packet[7:7+11], 2)
            self.subLenCount = subPacketCount
            subPacketString = self.packet[7+11:]
            print(str(self))
            while(subPacketCount > 0):
                subPacket = Packet(subPacketString)
                self.subPackets.append(subPacket)
                subPacketString = subPacket.remainder
                subPacketCount -= 1
            self.remainder = subPacketString

    def parseLiteralPacket(self):
        packetContent = self.packet[6:]
        literal = ""
        while True:
            group = packetContent[:5]
            packetContent = packetContent[5:]
            literal += group[1:]
            if(group[0] == '0'):
                self.remainder = packetContent
                break
        self.val = int(literal, 2)
        print(str(self))

    def __str__(self):
        if(self.packetType == LITERAL):
            return f"Ver[{self.version}]Type[{self.packetType}]Val[{self.val}]"
        else:
            return f"Ver[{self.version}]Type[{self.packetType}]LenType[{self.lenType}]LC[{self.subLenCount}]"


def countVersions(rootPacket: Packet):
    verCount = rootPacket.version
    # print(rootPacket)
    for packet in rootPacket.subPackets:
        verCount += countVersions((packet))
    return verCount


def mainLogic(data):
    print(data[0])
    data = bin(int(data[0], 16))[2:]
    if(len(data) % 4 != 0):
        data = ("0"*(4-(len(data) % 4))) + data
    packet = Packet(data)
    print(countVersions(packet))


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
