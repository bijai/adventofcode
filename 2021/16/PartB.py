from argparse import *
from re import M

SUM = 0
PRODUCT = 1
MINIMUM = 2
MAXIMUM = 3
LITERAL = 4
GREAT = 5
LESS = 6
EQUAL = 7

opMap = {
    -1:"UNKN",
    SUM: "SUM",
    PRODUCT: "PROD",
    MINIMUM: "MIN",
    MAXIMUM: "MAX",
    LITERAL: "LIT",
    GREAT: "GT",
    LESS: "LT",
    EQUAL: "EQ"
}

LENTYPE_TOTAL = 0
LENTYPE_COUNT = 1


class Packet:
    def __init__(self, packetString) -> None:
        self.packet = packetString
        self.version = -1
        self.remainder = ""
        self.val = -1
        self.subLenCount = -1
        self.subPackets = []
        self.lenType = -1
        self.packetType = -1
        self.decodePacket()

    def decodePacket(self):
        packet = self.packet
        self.version = int(packet[0:3], 2)
        self.packetType = int(packet[3:6], 2)

        if(self.packetType != LITERAL):
            self.parseOperatorPacket()
        else:
            self.parseLiteralPacket()

    def parseOperatorPacket(self):
        self.lenType = int(self.packet[6], 2)
        if(self.lenType == LENTYPE_TOTAL):
            subPacketlength = int(self.packet[7:7+15], 2)
            self.subLenCount = subPacketlength
            subPacketString = self.packet[7+15:7+15+subPacketlength]
            self.remainder = self.packet[7+15+subPacketlength:]
            print(str(self))
            while len(subPacketString) > 0:
                subPacket = Packet(subPacketString)
                self.subPackets.append(subPacket)
                subPacketString = subPacket.remainder

        elif(self.lenType == LENTYPE_COUNT):
            subPacketCount = int(self.packet[7:7+11], 2)
            self.subLenCount = subPacketCount
            subPacketString = self.packet[7+11:]
            print(str(self))
            while(subPacketCount > 0):
                subPacket = Packet(subPacketString)
                self.subPackets.append(subPacket)
                subPacketString = subPacket.remainder
                subPacketCount -= 1
            self.remainder = subPacketString

    def parseLiteralPacket(self):
        packetContent = self.packet[6:]
        literal = ""
        while True:
            group = packetContent[:5]
            packetContent = packetContent[5:]
            literal += group[1:]
            if(group[0] == '0'):
                self.remainder = packetContent
                break
        self.val = int(literal, 2)
        print(str(self))

    def __str__(self):
        if(self.packetType == LITERAL):
            return f"Ver[{self.version}]Type[{opMap[self.packetType]}]Val[{self.val}]"
        else:
            return f"Ver[{self.version}]Type[{opMap[self.packetType]}]LenType[{self.lenType}]LC[{self.subLenCount}]"


def countVersions(rootPacket: Packet):
    verCount = rootPacket.version
    # print(rootPacket)
    for packet in rootPacket.subPackets:
        verCount += countVersions((packet))
    return verCount


def resolve(packet: Packet):
    if packet.packetType == LITERAL:
        return packet.val
    elif packet.packetType == SUM:
        sum = 0
        for subpacket in packet.subPackets:
            sum += resolve(subpacket)
        return sum
    elif packet.packetType == PRODUCT:
        product = 1
        for subpacket in packet.subPackets:
            product *= resolve(subpacket)
        return product
    elif packet.packetType == MINIMUM:
        min = 9999999999
        for subpacket in packet.subPackets:
            val = resolve(subpacket)
            if(val < min):
                min = val
        return min
    elif packet.packetType == MAXIMUM:
        max = 0
        for subpacket in packet.subPackets:
            val = resolve(subpacket)
            if(val > max):
                max = val
        return max
    elif packet.packetType == GREAT:
        return 1 if resolve(packet.subPackets[0]) > resolve(packet.subPackets[1]) else 0
    elif packet.packetType == LESS:
        return 1 if resolve(packet.subPackets[0]) < resolve(packet.subPackets[1]) else 0
    elif packet.packetType == EQUAL:
        return 1 if resolve(packet.subPackets[0]) == resolve(packet.subPackets[1]) else 0


def mainLogic(data):
    print(data[0])
    finalLen = len(data[0]) * 4
    data = bin(int(data[0], 16))[2:]
    if(len(data)<finalLen):
        data = ("0"*(finalLen - len(data))) + data
    packet = Packet(data)

    print(resolve(packet))


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    parser.add_argument("-test", "-t", default=None)
    args = parser.parse_args()

    if(args.test is not None):
        cmdList = []
        cmdList.append(args.test)
        return  cmdList

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
