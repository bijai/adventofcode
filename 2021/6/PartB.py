from argparse import *



def mainLogic(data):
    fishes = [int(x) for x in data[0].split(',')]
    fishesDict = {0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0,8:0}
    for fish in fishes:
        if( fish in fishesDict):
            fishesDict[fish]+=1
        else :
            fishesDict[fish] = 1
    

    dayCount = 256
    for i in range(dayCount):
        
        fishesGoingToSplit = fishesDict[0]
        for j in range(8):
            fishesDict[j]=fishesDict[j+1]
        fishesDict[8] = fishesGoingToSplit
        fishesDict[6]+=fishesGoingToSplit

        print(f'Day {i}')# : {fishes}')
    count =0
    for fishDay in fishesDict:
        count+=fishesDict[fishDay]
    print(count)



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
