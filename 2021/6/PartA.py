from argparse import *



def mainLogic(data):
    fishes = [int(x) for x in data[0].split(',')]

    dayCount = 80
    for i in range(dayCount):
        for idx,fish in enumerate(fishes):
            if(fish == 0):
                fishes.append(9)
                fishes[idx] = 6
            else:
                fishes[idx]-=1
        #print(f'Day {i} : {fishes}')
    print(len(fishes))


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
