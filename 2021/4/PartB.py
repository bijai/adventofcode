

from argparse import *
import copy


def initBoard():
    return {
        "elem": {},
        "rowVec": {},
        "colVec": {},
        "raw":[]
    }

def parseBoards(data):
    boards = []
    board = initBoard()
    row = 0
    for line in data:
        if(line != ''):
            colArr = [int(x) for x in line.split()]
            board['rowVec'][row] = 0
            for col,item in enumerate(colArr):
                board['colVec'][col] = 0
                board['elem'][item] = {
                    "x":row,
                    "y":col,
                    "marked":False,
                }
            board["raw"].append(colArr)
            row+=1
        else:
            boards.append(board)
            board = initBoard()
            row = 0
    boards.append(board)
    return boards

### returns (numPresent,strikeFound)
def markNumber(board,num):
    marked = False
    strike=False
    if(num in board['elem']):
        board['elem'][num]["marked"] = True
        board['rowVec'][board['elem'][num]["x"]] += 1
        if(board['rowVec'][board['elem'][num]["x"]] == 5):
            strike = True

        board['colVec'][board['elem'][num]["y"]] += 1
        if(board['colVec'][board['elem'][num]["y"]] == 5):
            strike = True
        marked = True
    return(marked,strike)

def sumUnmarked(board):
    total = 0
    for elem in board['elem']:
        if not board['elem'][elem]['marked']:
            total+=elem
    return total


def mainLogic(data):
    # print(data)
    numbersDrawn = [int(x) for x in data[0].split(',')]
    boards = parseBoards(data[2:])

    lastWinBoard = None
    lastWinNum = None
    completedBoard = set()
    for num in numbersDrawn:
        print(num,end=' ')
        for idx,board in enumerate(boards):
            if(idx in completedBoard):
                continue
            marked,strike = markNumber(board,num)
            if(strike):
                lastWinBoard = copy.deepcopy(board)
                lastWinNum = num
                completedBoard.add(idx)
    
    print(lastWinBoard)
    print(sumUnmarked(lastWinBoard))
    print(lastWinNum)
    print(sumUnmarked(lastWinBoard)*lastWinNum)


    
        
    



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
