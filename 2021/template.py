from argparse import *



def mainLogic(data):
    print(data)
  

def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    parser.add_argument("-test", "-t", default=None)
    args = parser.parse_args()

    if(args.test is not None):
        cmdList = []
        cmdList.append(args.test)
        return  cmdList

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
