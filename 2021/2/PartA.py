

from argparse import *


def mainLogic(data):
    #print(data)
    position = (0,0)

    for command in data:
        if('forward' == command[0]):
            position = (position[0]+int(command[1]),position[1])
        elif ('down' == command[0]):
            position = (position[0],position[1]+int(command[1]))
        elif ('up' == command[0]):
            position = (position[0],position[1]-int(command[1]))
        else:
            raise Exception(f'Invalid command {command}')

    print(position,position[0]*position[1])



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()


    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.split() for x in lines]
    return commands

if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)