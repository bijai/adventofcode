from argparse import *

maxX = 0
maxY = 0

def printGrid(p_dict,X,Y):
    count = 0
    #print("\t"," ".join([str(x) for x in range(X+1)]))
    for y in range(Y+1):
        #print(y,end="\t")
        for x in range(X+1):
            if(x not in p_dict or  y not in p_dict[x]):
                pass
                #print('.',end=' ')
            else:
                #print(p_dict[x][y],end=' ')
                if( p_dict[x][y] > 0):
                    count+=1
        #print('')
    return count


def plotPoint(p_dict,x,y):
    global maxX,maxY
    if x not in p_dict:
        p_dict[x] = {}
        if x > maxX:
            maxX = x
    if y not in p_dict[x]:
        p_dict[x][y] = 1
        if y > maxY:
            maxY = y
    else:
        p_dict[x][y]+=1

def foldGrid(pointsDict,splitAxis,splitLine):
    global maxX,maxY
    
    if(splitAxis == 'y'):
        for x in list(pointsDict):
            for y in list(pointsDict[x]):
                if y > splitLine:
                    newY = 2*splitLine - y
                    #print(x,y,newY)
                    plotPoint(pointsDict,x,newY)
                    del pointsDict[x][y]
        maxY = splitLine
    elif(splitAxis == 'x'):
        for x in list(pointsDict):
            for y in list(pointsDict[x]):
                if x > splitLine:
                    newX = 2*splitLine - x
                    #print(x,y,newX)
                    plotPoint(pointsDict,newX,y)
                    del pointsDict[x][y]
        maxX = splitLine
        

def mainLogic(data):
    global maxX,maxY
    
    pointsDict = {}

    lastPointLine = 0

    for idx,line in enumerate(data):
        if len(line)==0:
            lastPointLine = idx
            break
        x,y = [int(x) for x in line.split(',')]
        plotPoint(pointsDict,x,y)
    
    

    folds = data[lastPointLine+1:]

    for i,fold in enumerate(folds):
        folds[i] = fold.split()[2].split('=')
    
    printGrid(pointsDict,maxX,maxY)

    for fold in folds:
        splitAxis = fold[0]
        splitLine = int(fold[1])

        print(f"Folding {splitAxis} = {splitLine}")
        foldGrid(pointsDict,splitAxis,splitLine)
        print(f"New Dimensions {maxX},{maxY} ")

        count = printGrid(pointsDict,maxX,maxY)
        print(count)
        break
   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
