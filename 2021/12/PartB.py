from argparse import *

nodeDict = {}
pathCount = 0

class Node:
    def __init__(self,name) -> None:
        self.name = name
        self.paths = []
        self.big = name.isupper()

    def addPath(self,path) -> None:
        self.paths.append(path)

    def __str__(self) -> str:
        return f"{self.name} : [{self.big}] : [{self.paths}]"
    def __repr__(self) -> str:
        return self.__str__()

def createNodeIfNotExist(node_name):
    if node_name not in nodeDict:
        nodeDict[node_name] = Node(node_name)

def traverseCave(curr,visited,grace=False):
    global pathCount
    if(curr == 'end'):
        visited.append(curr)
        #print(visited)
        pathCount+=1
        return

    visited.append(curr)

    toVisit = []
    gracePaths = []
    for path in nodeDict[curr].paths:
        if(nodeDict[path].big):
            toVisit.append(path)
        elif path in visited and not grace and path !='start':
            toVisit.append(path)
            gracePaths.append(path)
        elif path not in visited:
            toVisit.append(path)
        

    for cave in toVisit:
        traverseCave(cave,visited[:],grace or cave in gracePaths)




def mainLogic(data):
    for line in data:
        l_from,l_to = line.split('-')
        
        createNodeIfNotExist(l_from)
        createNodeIfNotExist(l_to)

        nodeDict[l_from].addPath(l_to)
        nodeDict[l_to].addPath(l_from)

    traverseCave('start',[])
    print(pathCount)

    #print(nodeDict)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
