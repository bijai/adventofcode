from argparse import *
from collections import defaultdict
from queue import PriorityQueue
import os

V = 0
H = 1

INFINITY = 999999999999


def redText(text):
    return f"\033[91m{text}\033[0m"


def getCost(mapData, xy):
    maxVal = len(mapData)
    baseCost = mapData[xy[V] % maxVal][xy[H] % maxVal]
    addition = int(xy[V]/maxVal) + int(xy[H]/maxVal)
    finalCost = baseCost + addition
    if finalCost < 10:
        return finalCost
    else:
        return finalCost%10 + 1


def getHCost(currNode, endNode):
    return (abs(endNode[V] - currNode[V]) + abs(endNode[H] - currNode[H]))


def printMap(mapData, visitedNode, currNode=None):
    os.system('cls')
    for v in range(len(mapData)*5):
        if(v%len(mapData) == 0):
            print("")
        for h in range(len(mapData)*5):
            if(h%len(mapData) == 0):
                print(" ",end='')
            cost = getCost(mapData, (v, h))
            if(currNode == (v, h)):
                print("O", end="")
            else:
                print(cost if (v, h) not in visitedNode else redText(
                    cost), end="")
        print("")
    print('')


def constant_factory(value):
    return lambda: value


def make_path(mapData, cameFrom, node):
    path = []
    pathCost = 0

    path.append(node)
    pathCost += getCost(mapData, node)

    while node in cameFrom:
        node = cameFrom[node]
        path.append(node)
        pathCost += getCost(mapData, node)

    pathCost -= getCost(mapData, node)

    return (path, pathCost)


def getNeighbors(mapData, currNode):
    nodeToVisit = []
    # go right
    if(currNode[H] < len(mapData)*5 - 1):
        nodeToVisit.append((currNode[V], currNode[H]+1))

    # go down
    if(currNode[V] < len(mapData)*5 - 1):
        nodeToVisit.append((currNode[V]+1, currNode[H]))

    # go left
    if(currNode[H] > 0):
        nodeToVisit.append((currNode[V], currNode[H]-1))

    # go up
    if(currNode[V] > 0):
        nodeToVisit.append((currNode[V]-1, currNode[H]))

    return nodeToVisit


def aStarAlgo(mapData, startNode, endNode):
    # Algo from 
    # https://en.wikipedia.org/wiki/A*_search_algorithm

    openSet = PriorityQueue()

    cameFrom = {}

    gScore = defaultdict(constant_factory(INFINITY))
    gScore[startNode] = 0

    fScore = defaultdict(constant_factory(INFINITY))
    fScore[startNode] = getHCost(startNode, endNode)
    openSet.put((fScore[startNode],startNode))

    while not openSet.empty():
        pCurrNode = openSet.get()
        currNode = pCurrNode[1]
        if(currNode == endNode):
            return make_path(mapData, cameFrom, currNode)
        for neighbor in getNeighbors(mapData, currNode):
            t_gScore = gScore[currNode] + getCost(mapData, neighbor)
            if t_gScore < gScore[neighbor]:
                cameFrom[neighbor] = currNode
                gScore[neighbor] = t_gScore
                fScore[neighbor] = t_gScore + getHCost(neighbor, endNode)
                # Optimize ? Linear list search - use another DS to hash index it ? 
                if((fScore[neighbor],neighbor) not in openSet.queue):
                    openSet.put((fScore[neighbor],neighbor))
    raise Exception('No path found')


def mainLogic(data):
    xLen = len(data)
    yLen = len(data[0])
    assert(xLen == yLen)
    path, pathCost = aStarAlgo(data, (0, 0), (xLen*5-1, xLen*5-1))
    printMap(data,path)

    #print(path)
    print(pathCost)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [[int(y) for y in x.strip()] for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
