'''
A recursive approach to A* like algo
However A* has context to update whether a 
previously discarded node is viable when 
approached from a different node.
With addition of couple of more DS 
we could bring same. 
Made me realize A* is really elegant. 
'''

from argparse import *
from collections import Counter, defaultdict
import os

V = 0
H = 1

gRouteMap = []
INFINITY = 999999999999
gBestCost = INFINITY

def constant_factory(value):
    return lambda: value

gNodeCost = {}
gNodePath = {}


def redText(text):
    return f"\033[91m{text}\033[0m"


def getCost(mapData, xy):
    return mapData[xy[V]][xy[H]]


def getWeightCost(mapData, xy, endNode):
    return (getCost(mapData, xy)) + 10 * (abs(endNode[V] - xy[V]) + abs(endNode[H] - xy[H]))


def printMap(mapData, visitedNode,currNode = None):
    os.system('cls')
    for v in range(len(mapData)):
        for h in range(len(mapData[v])):
            if(currNode == (v,h)):
                print("O",end="")
            else:
                print(mapData[v][h] if (v, h) not in visitedNode else redText(mapData[v][h]), end="")
        print("")
    print('')


def traverseMap(mapData, currNode, prevNodeSet: set, cost, endNode):
    global gBestCost
    printMap(mapData, prevNodeSet,currNode)
    cost += getCost(mapData, currNode)
    if(currNode == endNode):
        if(gBestCost < cost):
            return getCost(mapData, currNode)
        prevNodeSet.add(currNode)
        gRouteMap.append({"cost": cost, "map": prevNodeSet})
        print(cost)
        prevNodeSet.remove(currNode)
        if(gBestCost > cost):
            gBestCost = cost
        printMap(mapData, prevNodeSet)
        return getCost(mapData, currNode)

    if(cost > gBestCost):
        return cost

    prevNodeSet.add(currNode)

    nodeToVisit = []
    # go right
    nextNode = (currNode[V], currNode[H]+1)
    if(currNode[H] < len(mapData) - 1 and nextNode not in prevNodeSet):
        nodeToVisit.append(nextNode)

    # go down
    nextNode = (currNode[V]+1, currNode[H])
    if(currNode[V] < len(mapData) - 1 and nextNode not in prevNodeSet):
        nodeToVisit.append(nextNode)

    # go left
    nextNode = (currNode[V], currNode[H]-1)
    if(currNode[H] > 0 and nextNode not in prevNodeSet):
        nodeToVisit.append(nextNode)

    # go up
    nextNode = (currNode[V]-1, currNode[H])
    if(currNode[V] > 0 and nextNode not in prevNodeSet):
        nodeToVisit.append(nextNode)

    weightedNodes = {k: getWeightCost(mapData, k, endNode)
                     for k in nodeToVisit}
    weightedNodes = dict(
        sorted(weightedNodes.items(), key=lambda item: item[1]))
    routeCost = INFINITY
    gNodeCost[currNode] = INFINITY
    for node in weightedNodes:
        if(node in gNodeCost and gNodeCost[node] != INFINITY):
            routeCost = gNodeCost[node]
        elif (node in gNodeCost and  gNodeCost[node] == INFINITY):
            continue
        else:
            routeCost = traverseMap(mapData, node, prevNodeSet, cost, endNode)
        
        if(routeCost + getCost(mapData, currNode) < gNodeCost[currNode]):
            gNodeCost[currNode] = routeCost + getCost(mapData, currNode) 
            gNodePath[currNode] = node


    prevNodeSet.remove(currNode)
    return gNodeCost[currNode]


def mainLogic(data):
    global gBestCost
    xLen = len(data)
    yLen = len(data[0])
    assert(xLen == yLen)
    traverseMap(data, (0, 0), set(), 0, (xLen-1, xLen-1))

    print(len(gRouteMap))
    print(gBestCost)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [[int(y) for y in x.strip()] for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
