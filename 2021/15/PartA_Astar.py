from argparse import *
from collections import defaultdict
import os

V = 0
H = 1

INFINITY = 999999999999



def redText(text):
    return f"\033[91m{text}\033[0m"


def getCost(mapData, xy):
    return mapData[xy[V]][xy[H]]


def getHCost(currNode, endNode):
    return (abs(endNode[V] - currNode[V]) + abs(endNode[H] - currNode[H]))


def printMap(mapData, visitedNode, currNode=None):
    os.system('cls')
    for v in range(len(mapData)):
        for h in range(len(mapData[v])):
            if(currNode == (v, h)):
                print("O", end="")
            else:
                print(mapData[v][h] if (v, h) not in visitedNode else redText(
                    mapData[v][h]), end="")
        print("")
    print('')


def constant_factory(value):
    return lambda: value


def make_path(mapData, cameFrom, node):
    path = []
    pathCost = 0

    path.append(node)
    pathCost += getCost(mapData, node)

    while node in cameFrom:
        node = cameFrom[node]
        path.append(node)
        pathCost += getCost(mapData, node)

    pathCost -= getCost(mapData, node)

    return (path,pathCost)



def getNeighbors(mapData, currNode):
    nodeToVisit = []
    # go right
    if(currNode[H] < len(mapData) - 1):
        nodeToVisit.append((currNode[V], currNode[H]+1))

    # go down
    if(currNode[V] < len(mapData) - 1):
        nodeToVisit.append((currNode[V]+1, currNode[H]))

    # go left
    if(currNode[H] > 0):
        nodeToVisit.append((currNode[V], currNode[H]-1))

    # go up
    if(currNode[V] > 0):
        nodeToVisit.append((currNode[V]-1, currNode[H]))

    return nodeToVisit


def aStarAlgo(mapData, startNode, endNode):
    openSet = set()
    openSet.add(startNode)

    cameFrom = {}

    gScore = defaultdict(constant_factory(INFINITY))
    gScore[startNode] = 0

    fScore = defaultdict(constant_factory(INFINITY))
    fScore[startNode] = getHCost(startNode, endNode)

    while len(openSet) > 0:
        currNode = sorted(openSet, key=lambda val: fScore[val])[0]
        if(currNode == endNode):
            return make_path(mapData, cameFrom, currNode)
        openSet.remove(currNode)
        for neighbor in getNeighbors(mapData, currNode):
            t_gScore = gScore[currNode] + getCost(mapData, neighbor)
            if t_gScore < gScore[neighbor]:
                cameFrom[neighbor] = currNode
                gScore[neighbor] = t_gScore
                fScore[neighbor] = t_gScore + getHCost(neighbor, endNode)
                if(neighbor not in openSet):
                    openSet.add(neighbor)
    raise Exception('No path found')


def mainLogic(data):
    global gBestCost
    xLen = len(data)
    yLen = len(data[0])
    assert(xLen == yLen)
    path,pathCost = aStarAlgo(data, (0, 0), (xLen-1, xLen-1))

    print(path)
    print(pathCost)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [[int(y) for y in x.strip()] for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
