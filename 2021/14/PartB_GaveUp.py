from argparse import *
from typing import Counter
import copy


class Node:
    __slots__ = ("data", "next")

    def __init__(self, p_data) -> None:
        self.data = p_data
        self.next = None


STEPS = 40


rulesDict = {}


def parseRules(rules):
    result = {}
    for rule in rules:
        r_split = rule.split(' -> ')
        result[r_split[0]] = r_split[1]
    return result


def stat(polymer,lastChar):
    elemDict = Counter()
    for pair in polymer:
        elemDict[pair[0]] += polymer[pair]
    elemDict[lastChar] +=1
    sortedResult = sorted(elemDict.items(), key=lambda kv: (kv[1], kv[0]))
    print(polymer)
    print(elemDict)
    print(sortedResult[-1][1]-sortedResult[0][1])



def expandPolymer(polyDict,rulesDict):
    polyDictNew = Counter()
    for pair in polyDict:
        for expandPair in rulesDict[pair]:
            polyDictNew[expandPair] += polyDict[pair]
    return polyDictNew



def genPolymerDict(startPolymer):

    polyDict = Counter()
    for idx, char in enumerate(startPolymer[:-1]):
        pair = char + startPolymer[idx+1]
        polyDict[pair] += 1
    return polyDict


def mainLogic(data):
    global rulesDict

    startPolymer = data[0]
    rules = data[2:]
    rulesDict = parseRules(rules)
    rulesDict = {key: [f"{key[0]}{rulesDict[key]}",
                       f"{rulesDict[key]}{key[1]}"] for key in rulesDict}

    #print(rulesDict)

    polymer = genPolymerDict(startPolymer)

    for _ in range(STEPS):
        polymer = expandPolymer(polymer,rulesDict)
    stat(polymer,startPolymer[-1])






def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
