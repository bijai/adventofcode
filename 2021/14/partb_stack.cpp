#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <string>
#include <ctime>

using namespace std;

unordered_map<char, long long> g_elem_dict;
int Gmax = 0;
char GmaxElem = '0';
char GminElem = '0';
int Gmin = 999999999;

long long  GTotal = 0;
long long  GCurr = 0;
time_t lastPrintTime = time(0);

struct Node
{
    Node *next = nullptr;
    char data;

    Node(char p_data) : data(p_data)
    {
    }
};

void printLinkedList(Node *rootNode)
{
    Node *curr;
    curr = rootNode;
    while (curr != nullptr)
    {
        cout << curr->data;
        curr = curr->next;
    }
    cout << endl;
}

void stat_elem(char data)
{
    if (g_elem_dict.find(data) != g_elem_dict.end())
    {
        g_elem_dict[data]++;
    }
    else
    {
        g_elem_dict[data] = 1;
    }

    if (g_elem_dict[data] < Gmin)
    {
        GminElem = data;
        Gmin = g_elem_dict[data];
    }
    else if (GminElem == data) // update count if same elem
    {
        Gmin = g_elem_dict[data];
    }

    if (g_elem_dict[data] > Gmax)
    {
        GmaxElem = data;
        Gmax = g_elem_dict[data];
    }
}

void stat(Node *polymerRoot)
{
    // printLinkedList(polymerRoot);
    Node *curr = polymerRoot;

    stat_elem(curr->data);
    stat_elem(curr->next->data);

    GCurr += 2;
    time_t curTime = time(0);
     //cout << time(0) << endl;
    if ((curTime - lastPrintTime) > 10)
    {
        // cout << curTime - lastPrintTime;
        //float percent = ((GCurr / GTotal) * 100);
        //cout << percent << "\r";
        cout << ((double)GCurr/(double)GTotal) * 100<< "\r";
        lastPrintTime = curTime;
    }
}

void Gstat(Node *polymerRoot)
{
    // take stat of last char
    Node *curr = polymerRoot;
    while (curr->next != nullptr)
    {
        curr = curr->next;
    }
    stat_elem(curr->data);

    cout << "Max [" << GmaxElem << "] (" << Gmax << ")\n";
    cout << "Min [" << GminElem << "] (" << Gmin << ")\n";
    cout << Gmax - Gmin << endl;

    for (const auto &itr : g_elem_dict)
    {
        cout << itr.first << " : " << itr.second << endl;
    }
}

void expandPolymer(Node *startNode, unordered_map<string, char> &rule_dict, int step, Node *stopNode = nullptr)
{
    if (0 == step)
    {
        stat(startNode);
        return;
    }
    Node *curr = startNode;
    while ((curr != nullptr) && (curr->next != nullptr))
    {

        string qString;
        qString += curr->data;
        qString += curr->next->data;

        if (nullptr == stopNode)
        {
            cout << qString << endl;
        }

        Node *newNode = new Node(rule_dict[qString]);
        newNode->next = curr->next;
        curr->next = newNode;
        // curr = newNode->next;

        expandPolymer(curr, rule_dict, step - 1, newNode->next);

        curr->next = newNode->next;
        delete newNode;
        newNode = nullptr;

        curr = curr->next;
        if (curr == stopNode)
            break;
    }
}

int main(int argc, char *argv[])
{
    string fName;
    if (argc < 3)
    {
        cout << "Usage " << argv[0] << " inputFile steps\n";
        return 1;
    }

    fName = argv[1];
    int steps = stoi(argv[2]);

    string fLine;

    ifstream fileObj(fName);

    vector<string> lines;
    while (getline(fileObj, fLine))
    {
        lines.push_back(fLine);
    }

    string startStr(lines[0]);

    lines.erase(lines.begin());
    lines.erase(lines.begin());

    unordered_map<string, char> rule_dict;

    for (const auto &itr : lines)
    {
        rule_dict[itr.substr(0, 2)] = itr.substr(6)[0];
    }

    cout << "Starting string [" << startStr << "] Steps [" << steps << "]\n";
    GTotal = (startStr.length() * pow(2, steps)) - pow(2, steps) + 1;
    //cout << lastPrintTime << endl;

    Node *polymerRoot = new Node(startStr[0]);
    Node *curr = polymerRoot;

    for (const auto &itr : startStr.substr(1))
    {
        Node *newNode = new Node(itr);
        curr->next = newNode;
        curr = newNode;
    }

    printLinkedList(polymerRoot);

   

    expandPolymer(polymerRoot, rule_dict, steps);

    Gstat(polymerRoot);

    return 0;
}