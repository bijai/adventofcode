from argparse import *

class Node:
    __slots__ = ("data","next")
    def __init__(self,p_data) -> None:
        self.data = p_data
        self.next = None




STEPS = 40

rulesDict = {}

def parseRules(rules):
    result = {}
    for rule in rules:
        r_split = rule.split(' -> ')
        result[r_split[0]] = r_split[1]
    return result

def stat(polymer):
    elemDict = {}

    curr = polymer
    while curr is not None:
        elem = curr.data
        if(elem in elemDict):
            elemDict[elem]+=1
        else:
            elemDict[elem] = 1
        curr = curr.next
    sortedResult = sorted(elemDict.items(), key =
             lambda kv:(kv[1], kv[0]))
    print(elemDict)
    print(sortedResult[-1][1]-sortedResult[0][1])

def getExpandString(node):
    query = node.data
    if( node.next is None):
        return None
    query +=node.next.data
    return query

#@profile
def expandPolymer(polymerRoot,steps):
    for step in range(steps):
        curr = polymerRoot
        while(curr != None and curr.next != None):
            q_string = getExpandString(curr)
            if q_string is None:
                break
            newNode = Node(rulesDict[q_string])
            newNode.next = curr.next
            curr.next = newNode
            curr = newNode.next
        print(step)


    return polymerRoot
        
        

def genLinkedList(str_data):
    rootNode = Node(str_data[0])
    curr = rootNode
    for elem in str_data[1:]:
        newNode = Node(elem)
        curr.next = newNode
        curr = newNode

    return rootNode

def printLinkedList(rootNode):
    curr = rootNode
    while(None != curr):
        print(curr.data,end="")
        curr = curr.next
    print("")



    

def mainLogic(data):
    global rulesDict

    template = data[0]
    rules = data[2:]
    rulesDict = parseRules(rules)

    polymer = genLinkedList(template)

    printLinkedList(polymer)
    polymer = expandPolymer(polymer,STEPS)
    #printLinkedList(polymer)
    #print(polymer)
    stat(polymer)


   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
