from argparse import *

from multiprocessing import Process, Lock

STEPS = 40


MAX_THREAD = 11



finalCount = {}


def expandPolymer(threadID,rulesDict,polymer,threadLock):
    for step in range(STEPS):
        new_polymer = polymer[0]
        for i in range(len(polymer)-1):
            pair = polymer[i:i+2]
            if(pair in rulesDict):
                new_polymer+= rulesDict[pair] + polymer[i+1]
            else:
                new_polymer+=polymer[i+1]
        polymer = new_polymer
        print(f"Thread[{threadID}] : Step {step+1} : {len(polymer)}")
    if(threadID != 0):
        elemDict = stat(new_polymer[1:])
    else:
        elemDict = stat(new_polymer)

    threadLock.acquire()
    for elem in elemDict:
        if elem in finalCount:
            finalCount[elem]+=elemDict[elem]
        else:
            finalCount[elem]=elemDict[elem]
    threadLock.release()
    print(f"Done : T[{threadID}] : [{len(new_polymer)}]")


def parseRules(rules):
    result = {}
    for rule in rules:
        r_split = rule.split(' -> ')
        result[r_split[0]] = r_split[1]
    return result

def stat(polymer):
    elemDict = {}
    for elem in polymer:
        if(elem in elemDict):
            elemDict[elem]+=1
        else:
            elemDict[elem] = 1
    return elemDict

def mainLogic(data):
    #print(data)

    template = data[0]
    rules = data[2:]
    rulesDict = parseRules(rules)

    polymer = template

    print(polymer)
    
    p_split_len = int(len(polymer)/MAX_THREAD)
    print(p_split_len)
    p_split_arr = []
    currIdx = 1
    while(currIdx+p_split_len <= len(polymer)):
        p_split_arr.append(polymer[currIdx-1:currIdx+p_split_len])
        currIdx+=p_split_len
    p_split_arr[-1] = p_split_arr[-1]+polymer[currIdx:]

    print (p_split_arr)

    input()
    threadLock = Lock()

    threadArr = []

    for idx,line in enumerate(p_split_arr):
        threadArr.append(Process(target=expandPolymer, args=(idx, rulesDict,line,threadLock)))
        threadArr[-1].start()

    for thread in threadArr:
        thread.join()
    print ("All threads joined")

    print(finalCount)

    sortedResult = sorted(finalCount.items(), key =
             lambda kv:(kv[1], kv[0]))
    print(sortedResult[-1][1]-sortedResult[0][1])
    


   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
