from argparse import *

STEPS = 2

rulesDict = {}

def parseRules(rules):
    result = {}
    for rule in rules:
        r_split = rule.split(' -> ')
        result[r_split[0]] = r_split[1]
    return result

def stat(polymer):
    elemDict = {}
    for elem in polymer:
        if(elem in elemDict):
            elemDict[elem]+=1
        else:
            elemDict[elem] = 1
    sortedResult = sorted(elemDict.items(), key =
             lambda kv:(kv[1], kv[0]))
    print(elemDict)
    print(sortedResult[-1][1]-sortedResult[0][1])


memCache = {}




def expandPolymer(polymer,currStep):
    if currStep > STEPS:
        return polymer
    if(len(polymer) == 2):
        if(polymer in memCache):
            if(STEPS-currStep in memCache[polymer]):
                return memCache[polymer][STEPS-currStep]
            else:
                lastStepComputed = list(memCache[polymer])[-1]
                return expandPolymer(memCache[polymer][lastStepComputed],currStep+lastStepComputed)
        expansion = polymer[0] + rulesDict[polymer] + polymer[1]
        memCache[polymer] = {}
        memCache[polymer][1] = expansion
        l_temp = expandPolymer(expansion,currStep+1)
        memCache[polymer][STEPS-currStep] = l_temp
        return l_temp

    expansion = ""
            
    for i in range(len(polymer)-1):
        pair = polymer[i:i+2]
        expansion += expandPolymer(pair,currStep)[:-1]
    
    expansion+=polymer[-1]

    return expansion
        
        


    

def mainLogic(data):
    global rulesDict

    template = data[0]
    rules = data[2:]
    rulesDict = parseRules(rules)

    polymer = template

    print(polymer)
    polymer = expandPolymer(polymer,1)
    print(polymer)
    stat(polymer)


   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
