#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <string>

using namespace std;

struct Node
{
    Node *next = nullptr;
    char data;

    Node(char p_data) : data(p_data)
    {
    }
};

void printLinkedList(Node *rootNode)
{
    Node *curr;
    curr = rootNode;
    while (curr != nullptr)
    {
        cout << curr->data;
        curr = curr->next;
    }
    cout << endl;
}

void expandPolymer(Node *rootNode, unordered_map<string, char> &rule_dict)
{
    Node *curr = rootNode;
    while ((curr != nullptr) && (curr->next != nullptr))
    {
        string qString;
        qString += curr->data;
        qString += curr->next->data;

        Node *newNode = new Node(rule_dict[qString]);
        newNode->next = curr->next;
        curr->next = newNode;
        curr = newNode->next;
    }
}

void stat(Node *polymerRoot)
{
    unordered_map<char, int> elem_dict;
    Node *curr = polymerRoot;
    int max = 0;
    char maxElem = '0';
    char minElem = 'A';
    int min = 999999999;
    while (curr != nullptr)
    {
        if (elem_dict.find(curr->data) != elem_dict.end())
        {
            elem_dict[curr->data]++;
        }
        else
        {
            elem_dict[curr->data] = 1;
        }

        if (elem_dict[curr->data] < min)
        {
            minElem = curr->data;
            min = elem_dict[curr->data];
        }
        else if (minElem == curr->data) // update count if same elem
        {
            min = elem_dict[curr->data];
        }

        if (elem_dict[curr->data] > max)
        {
            maxElem = curr->data;
            max = elem_dict[curr->data];
        }

        curr = curr->next;
    }
    cout << std::flush;
    cout << "Max [" << maxElem << "] (" << max << ")\n";
    cout << "Min [" << minElem << "] (" << min << ")\n";
    cout << max - min << endl;
}

int main(int argc, char *argv[])
{
    string fName;
    if (argc < 3)
    {
        cout << "Usage " << argv[0] << " inputFile steps\n";
        return 1;
    }

    fName = argv[1];
    int steps = stoi(argv[2]);

    string fLine;

    ifstream fileObj(fName);
    vector<string> lines;
    while (getline(fileObj, fLine))
    {
        lines.push_back(fLine);
    }

    string startStr(lines[0]);

    lines.erase(lines.begin());
    lines.erase(lines.begin());

    unordered_map<string, char> rule_dict;

    for (const auto &itr : lines)
    {
        rule_dict[itr.substr(0, 2)] = itr.substr(6)[0];
    }

    cout << "Starting string [" << startStr << "] Steps [" << steps << "]\n";
    Node *polymerRoot = new Node(startStr[0]);
    Node *curr = polymerRoot;

    for (const auto &itr : startStr.substr(1))
    {
        Node *newNode = new Node(itr);
        curr->next = newNode;
        curr = newNode;
    }

    printLinkedList(polymerRoot);

    for (int i = 0; i < steps; i++)
    {
        cout << "Step [" << i + 1 << "]\n";
        expandPolymer(polymerRoot, rule_dict);
        // printLinkedList(polymerRoot);
    }

    stat(polymerRoot);

    return 0;
}