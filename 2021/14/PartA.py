from argparse import *

STEPS = 40

def parseRules(rules):
    result = {}
    for rule in rules:
        r_split = rule.split(' -> ')
        result[r_split[0]] = r_split[1]
    return result

def stat(polymer):
    elemDict = {}
    for elem in polymer:
        if(elem in elemDict):
            elemDict[elem]+=1
        else:
            elemDict[elem] = 1
    sortedResult = sorted(elemDict.items(), key =
             lambda kv:(kv[1], kv[0]))
    print(sortedResult[-1][1]-sortedResult[0][1])

    

def mainLogic(data):
    #print(data)

    template = data[0]
    rules = data[2:]
    rulesDict = parseRules(rules)

    polymer = template

    print(polymer)
    for step in range(STEPS):
        new_polymer = polymer[0]
        for i in range(len(polymer)-1):
            pair = polymer[i:i+2]
            if(pair in rulesDict):
                new_polymer+= rulesDict[pair] + polymer[i+1]
            else:
                new_polymer+=polymer[i+1]
        polymer = new_polymer
        #print(f"Step {step+1} : {polymer}")
        print(f"Step {step+1} : {len(polymer)}")
    
    stat(polymer)


   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
