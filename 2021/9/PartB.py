from argparse import *

basinDict = {}
def printGrid(data):
    riskLevel = 0
    for x,_ in enumerate(data):
        for y,_ in enumerate(data[x]):
            print(f"{data[x][y]['value']}{'*' if data[x][y]['low'] else ' '}",end="")
            if(data[x][y]["low"]):
                riskLevel+= data[x][y]["value"] + 1
        print(" ")
    print(riskLevel)

def findLowPoints(data):
    lowPointList = []
    for x,_ in enumerate(data):
        for y,_ in enumerate(data[x]):
            notLow = False
            try:
                if(data[x-1][y]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x+1][y]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x][y+1]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x][y-1]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            if not notLow:
                data[x][y]["low"] = True
                lowPointList.append((x,y))
    return lowPointList

def checkAndAddToBasin(grid,x,y,val,basin):
    global basinDict
    if x<0 or "basin" in grid[x][y]:
        return False
    if(grid[x][y]["value"] != 9 and grid[x][y]["value"] > val):
        grid[x][y]["basin"] = basin
        basinDict[basin] +=1
        #print(f"{basin} : {x},{y}")
        return True
    return False

def traverseBasin(grid,x,y,start=False,basin=""):
    global basinDict
    #print(x,y)
    if(start):
        basin = f"{x}_{y}"
        grid[x][y]["basin"] = basin
        if basin in basinDict:
            raise Exception("Intersecting basins ? ")
        basinDict[basin] = 1
    
    try:
        if checkAndAddToBasin(grid,x-1,y,grid[x][y]["value"],basin):
            traverseBasin(grid,x-1,y,start=False,basin=basin)
    except IndexError:
        pass

    try:
        if checkAndAddToBasin(grid,x+1,y,grid[x][y]["value"],basin):
            traverseBasin(grid,x+1,y,start=False,basin=basin)
    except IndexError:
        pass

    try:
        if checkAndAddToBasin(grid,x,y-1,grid[x][y]["value"],basin):
            traverseBasin(grid,x,y-1,start=False,basin=basin)
    except IndexError:
        pass

    try:
        if checkAndAddToBasin(grid,x,y+1,grid[x][y]["value"],basin):
            traverseBasin(grid,x,y+1,start=False,basin=basin)
    except IndexError:
        pass

    if(start):
        print(f"Basin stat [{basin}] : [{basinDict[basin]}]")






def mainLogic(data):
    global basinDict
    for idx,line in enumerate(data):
        data[idx] = [{"value":int(x),"low":False} for x in line]


    lowPoints = findLowPoints(data)
    printGrid(data)

    for point in lowPoints:
        #print(point)
        traverseBasin(data,point[0],point[1],start=True)

    result = [basinDict[x] for x in basinDict]
    result.sort(reverse=True)

    #print(result)
    print(result[0]*result[1]*result[2])

    
    
    

    
   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
