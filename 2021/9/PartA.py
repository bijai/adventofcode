from argparse import *

#1673 too high

def printGrid(data):
    riskLevel = 0
    for x,_ in enumerate(data):
        for y,_ in enumerate(data[x]):
            print(f"{data[x][y]['value']}{'*' if data[x][y]['low'] else ' '}",end="")
            if(data[x][y]["low"]):
                riskLevel+= data[x][y]["value"] + 1
        print(" ")
    print(riskLevel)

def findLowPoints(data):
    for x,_ in enumerate(data):
        for y,_ in enumerate(data[x]):
            notLow = False
            try:
                if(data[x-1][y]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x+1][y]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x][y+1]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            try:
                if(data[x][y-1]["value"] <= data[x][y]["value"]):
                    notLow = True
                    continue
            except IndexError:
                pass

            if not notLow:
                data[x][y]["low"] = True




def mainLogic(data):
    for idx,line in enumerate(data):
        data[idx] = [{"value":int(x),"low":False} for x in line]


    findLowPoints(data)
    printGrid(data)
    
    

    
   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
