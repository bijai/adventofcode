from argparse import *


def red(p_str):
    return '\x1b[1;31;40m' + str(p_str) + '\x1b[0m'


def printGrid(data):
    for line in data:
        print(f"".join([str(x) if x > 0 else red(x) for x in line]))

def printFlashedGrid(data):
    for x in data:
        for y in data[x]:
            print(f"{red(1) if data[x][y] else '0'}",end="")
        print('')


def incrNeighbour(data, x, y, flashed):
    flashed[x][y] = True
    for i in range(-1, 2):
        for j in range(-1, 2):
            x1 = x+i
            y1 = y+j
            if x1 < 0 or y1 < 0 or (i == 0 and j == 0):
                continue
            try:
                data[x1][y1] += 1
            except IndexError:
                continue
            if(data[x1][y1] > 9 and not flashed[x1][y1]):
                incrNeighbour(data, x1, y1, flashed)


# perform step return flash count


def performStep(data):
    flashed = dict.fromkeys(
        range(len(data)))
    for x in flashed:
        flashed[x] = dict.fromkeys(range(len(data[0])), False)

    for x, _ in enumerate(data):
        for y, _ in enumerate(data[x]):
            data[x][y] += 1
            if data[x][y] > 9 and not flashed[x][y]:
                incrNeighbour(data, x, y, flashed)
    flashCount = 0
    for x, _ in enumerate(data):
        for y, _ in enumerate(data[x]):
            if data[x][y] > 9:
                flashCount += 1
                data[x][y] = 0
    #printFlashedGrid(flashed)
    return flashCount


def mainLogic(data):
    for idx, line in enumerate(data):
        data[idx] = [int(x) for x in line]

    flash = 0
    for i in range(100):
        flash += performStep(data)
        print(i+1, flash)
        printGrid(data)
        print('-'*10)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample2.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
