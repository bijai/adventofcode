from argparse import *

grid = {}
maxX = 0
maxY = 0

def getPoint(x, y):
    if(x in grid):
        if(y in grid[x]):
            return grid[x][y]
    return 0


def incrPoint(x, y):
    global maxX,maxY
    if not x in grid:
        grid[x] = {}
        if(x > maxX):
            maxX = x
    if not y in grid[x]:
        grid[x][y] = 0
        if(y > maxY):
            maxY = y
    grid[x][y]+=1

def parsePoints(pointStr):
    pointList = [x.strip() for x in pointStr.split("->")]
    
    a = [int(x) for x in pointList[0].split(',')]
    b = [int(x) for x in pointList[1].split(',')]

    return a,b


def markLine(a,b):
    if(a[0]==b[0]):
        if a[1] < b[1]:
            min = a[1]
            max = b[1]+1
        else:
            min = b[1]
            max = a[1]+1
        for i in range(min,max):
            incrPoint(a[0],i)
    elif(a[1]==b[1]):
        if a[0] < b[0]:
            min = a[0]
            max = b[0]+1
        else:
            min = b[0]
            max = a[0]+1
        for i in range(min,max):
            incrPoint(i,a[1])
    else:
        x = b[1]-a[1]
        y = b[0]-a[0]
        c = (y*a[1] - x*a[0])/y 
        if a[0] < b[0]:
            min = a[0]
            max = b[0]+1
        else:
            min = b[0]
            max = a[0]+1
        for i in range(min,max):
            y1 = ((x/y)*i)+c
            incrPoint(i,y1)

def printBoard():
    global maxX,maxY
    count = 0
    for y in range(maxY+1):
        for x in range(maxX+1):
            p = getPoint(x,y)
            #print(p,end=' ')
            if(p > 1):
                count+=1
        print(' ')
    return count

def mainLogic(data):
    for line in data:
        a,b = parsePoints(line)
        print(a,b)
        markLine(a,b)
    count = printBoard()
    print(count)

        #input("Anyket")


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
