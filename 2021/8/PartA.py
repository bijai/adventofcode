from argparse import *

'''
0 : 6
1 : 2 x
2 : 5
3 : 5
4 : 4 x
5 : 5
6 : 6
7 : 3 x
8 : 7 x
9 : 6

'''


def mainLogic(data):
    outputValues = []
    for line in data:
        outputValues.append(line.split('|')[1].split())

    count = 0
    for outputvalue in outputValues:
        for digit in outputvalue:
            if len(digit) == 2 or len(digit) == 3 or len(digit) == 4 or len(digit) == 7:
                count+=1
    print(count)

   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
