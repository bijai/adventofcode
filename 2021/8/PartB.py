from argparse import *
from copy import copy

'''
0 : 6
1 : 2 x
2 : 5
3 : 5
4 : 4 x
5 : 5
6 : 6
7 : 3 x
8 : 7 x
9 : 6

'''

def lenSort(key1):
    return len(key1)

def sortAndStringify(arr):
    arr.sort()
    arr = "".join(arr)
    return arr

def formSegDict(top,middle,bottom,top_left,top_right,bottom_left,bottom_right):
    result = {}

    #0
    result[sortAndStringify([top,bottom,top_left,top_right,bottom_left,bottom_right])] = 0

    #1
    result[sortAndStringify([top_right,bottom_right])] = 1

    #2
    result[sortAndStringify([top,middle,bottom,top_right,bottom_left])] = 2

    #3
    result[sortAndStringify([top,middle,bottom,top_right,bottom_right])] = 3

    #4
    result[sortAndStringify([middle,top_left,top_right,bottom_right])] = 4

    #5
    result[sortAndStringify([top,middle,bottom,top_left,bottom_right])] = 5

    #6
    result[sortAndStringify([top,middle,bottom,top_left,bottom_left,bottom_right])] = 6

    #7
    result[sortAndStringify([top,top_right,bottom_right])] = 7

    #8
    result[sortAndStringify([top,middle,bottom,top_left,top_right,bottom_left,bottom_right])] = 8

    #9
    result[sortAndStringify([top,middle,bottom,top_left,top_right,bottom_right])] = 9

    return result

def arrayToNum(arr):
    result = 0
    for digit in arr:
        result= result*10 + digit
    return result
def mainLogic(data):
    count = 0
    for line in data:
        jumbled,output = line.split('|')
        jumbled = jumbled.split()
        jumbled = [sorted(x) for x in jumbled]
        jumbled.sort(key=lenSort)
        #0  1   2   3   4   5   6   7   8   9   
        #2  3   4   5   5   5   6   6   6   7   
        
        print(jumbled)

        #segment present in 7 but not in 1 
        top = [item for item in jumbled[1] if item not in jumbled[0]][0]
        
        # 2,3 & 5 have all horizontal segments common (all three 5 segment lit up (3,4,5) of the array)
        horizontal = set(jumbled[3]) & set(jumbled[4]) & set(jumbled[5]) 

        # 4 only has the middle segment lit up
        middle = [x for x in horizontal if x in jumbled[2]][0]

        # since we know top and middle
        bottom = [x for x in horizontal if x not in [top,middle]][0]

        print(f"Top :[{top}]\nMiddle :[{middle}]\nBottom :[{bottom}]")

        # we know all except top left for 4 
        temp_arr = copy(jumbled[0])
        temp_arr.append(middle)
        top_left = [x for x in jumbled[2] if x not in temp_arr][0]

        print(f"Top Left :[{top_left}]")
        
        # out of 5 segment numbers, only one digit (5) have top left lit up 
        # for 5 we know all except bottom right 
        bottom_right = ''
        for i in range(3,6):
            if top_left in jumbled[i]:
                bottom_right = [x for x in jumbled[i] if x not in [top,middle,bottom,top_left]][0]
                break
        
        print(f'Bottom Right : [{bottom_right}]')

        # Bottom right and Top right make up the digit 1
        top_right = [x for x in jumbled[0] if x not in [bottom_right]][0]
        
        print(f'Top Right : [{top_right}]') 

        # the only segment left from 8 
        temp_arr = [top,middle,bottom,top_right,top_left,bottom_right]
        bottom_left = [x for x in jumbled[-1] if x not in temp_arr][0]

        print(f'Bottom Left : [{bottom_left}]')


        segDict = formSegDict(top,middle,bottom,top_left,top_right,bottom_left,bottom_right)
        print(segDict)

        output = output.split()
        output = [''.join(sorted(x)) for x in output]
        output = [ segDict[x] for x in output]
        outputNum = arrayToNum(output)
        count +=outputNum
        print(outputNum,count)



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
