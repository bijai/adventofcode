

from argparse import *

def computeIndexMax(pArray,index):
    count = {'0':0,'1':0}
    for code in pArray:
        count[code[index]] += 1
    return '0' if count['0'] > count['1'] else '1'

def computeIndexMin(pArray,index):
    count = {'0':0,'1':0}
    for code in pArray:
        count[code[index]] += 1
    return '0' if count['0'] <= count['1'] else '1'

def filterIdxChar(data,filterChar,idx):
    return [x for x in data if x[idx] == filterChar]


def mainLogic(data):
    # print(data)
    maxIndex = len(data[0])
    maxData = [x for x in data]
    for idx in range(maxIndex):
        if(len(maxData) == 1):
            break
        maxChar = computeIndexMax(maxData,idx)
        maxData = filterIdxChar(maxData,maxChar,idx)


    minData = [x for x in data]
    for idx in range(maxIndex):
        if(len(minData) == 1):
            break
        minChar = computeIndexMin(minData,idx)
        minData = filterIdxChar(minData,minChar,idx) 

    ogr = maxData[0]
    ogr = int(ogr,2)

    csr = minData[0]
    csr = int(csr,2)

    print(ogr*csr)



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
