

from argparse import *

def bitInvert(bits):
    result = ''
    for x in bits:
        if(x == '0'):
            result+='1'
        else :
            result+='0'
    return result

def computeIndexDict(pArray):
    maxIndex = len(pArray[0])
    indexDict = {}
    for index in range(maxIndex):
        indexDict[index] = {'0': 0, '1': 0}
    for code in pArray:
        for index in range(maxIndex):
            indexDict[index][code[index]] += 1
    return indexDict


def mainLogic(data):
    # print(data)
    indexDict = computeIndexDict(data)
    #print(indexDict)
    gammaRateStr = ''
    for index in indexDict:
        if(indexDict[index]['0'] > indexDict[index]['1']):
            gammaRateStr += '0'
        else:
            gammaRateStr += '1'
    epsilonRateStr = bitInvert(gammaRateStr)
    print(gammaRateStr)
    print(epsilonRateStr)
    gammaRate = int(gammaRateStr,2)
    epsilonRate = int(epsilonRateStr,2)
    print(gammaRate,epsilonRate,gammaRate*epsilonRate)


def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
