from argparse import *

opening = ['(','{','<','[']
closing = [')','}','>',']']

scoreDict = {
    ')':3,
    ']':57,
    '}':1197,
    '>':25137
}

syncDict = {}
for i,_ in enumerate(opening):
    syncDict[closing[i]] = opening[i]
    syncDict[opening[i]] = closing[i]


print(syncDict)

def mainLogic(data):
    print(data)
    score = 0
    for line in data:
        stack = []
        flag = False
        for idx,char in enumerate(line):
            if char in opening:
                stack.append(char)
            elif char in closing:
                elem = stack.pop()
                if elem != syncDict[char]:
                    print(f'Error found {idx} : {line}, Expected [{syncDict[elem]}] found [{char}]')
                    flag = True
                    score+=scoreDict[char]
                    break
        if not flag:
            print(f"No Error : {line}")
    print(score)



   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
