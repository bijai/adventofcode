from argparse import *

opening = ['(','{','<','[']
closing = [')','}','>',']']

scoreDict = {
    ')':3,
    ']':57,
    '}':1197,
    '>':25137
}

'''
): 1 point.
]: 2 points.
}: 3 points.
>: 4 points.
'''

closeScoreDict = {
    ')':1,
    ']':2,
    '}':3,
    '>':4
}

syncDict = {}
for i,_ in enumerate(opening):
    syncDict[closing[i]] = opening[i]
    syncDict[opening[i]] = closing[i]


def completeLine(stack):
    score = 0
    while len(stack)>0:
        elem = stack.pop()
        elem = syncDict[elem]
        score = score*5 + closeScoreDict[elem]
    return score



def mainLogic(data):
    #print(data)
    score = []
    for line in data:
        stack = []
        flag = False
        for char in line:
            if char in opening:
                stack.append(char)
            elif char in closing:
                elem = stack.pop()
                if elem != syncDict[char]:
                    #print(f'Error found : {line}, Expected [{syncDict[elem]}] found [{char}]')
                    flag = True
                    break
        if not flag:
            #print(f"No Error : {line}")
            if len(stack) > 0:
                #incomplete
                score.append(completeLine(stack))
    score.sort()

    print(score[int(len(score)/2)])



   



def readAndProcessInputs():
    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="input.txt")
    args = parser.parse_args()

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
