

from argparse import *

parser = ArgumentParser()
parser.add_argument("-file", "-f", default="input.txt")
args = parser.parse_args()


with open(args.file) as f:
    lines = f.readlines()

depths = [int(x.strip()) for x in lines]

WINDOW_SIZE = 3
prevDepthCompute = sum(depths[:WINDOW_SIZE])
prevDepth = depths[0]
print(prevDepthCompute)
incrCount = 0
for index, curDepth in enumerate(depths[WINDOW_SIZE:], start=WINDOW_SIZE):

    curDepthCompute = prevDepthCompute - depths[index-WINDOW_SIZE] + curDepth
    print(index, prevDepthCompute, curDepthCompute, curDepth, depths[index-WINDOW_SIZE])
    if(curDepthCompute > prevDepthCompute):
        incrCount += 1
    prevDepthCompute = curDepthCompute

print(incrCount)
