

with open("input.txt") as f:
    lines = f.readlines()

depths = [int(x.strip()) for x in lines]

prevDepth = depths[0]
incrCount = 0
for curDepth in depths[1:]:
    if(curDepth > prevDepth):
        incrCount += 1
    prevDepth = curDepth

print(incrCount)


