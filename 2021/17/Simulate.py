from argparse import *
import re
from tkinter import INSIDE

regexPatt = re.compile(
    r'target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)')

X = 0
Y = 1

OUTSIDE = 0
ONLY_X = 1
ONLY_Y = 2
INSIDE = 3


gMin = [0, 0]
gMax = [0, 0]


def recordBounds(pos):
    for i in range(2):
        if(pos[i] < gMin[i]):
            gMin[i] = pos[i]
        if(pos[i] > gMax[i]):
            gMax[i] = pos[i]


def computePos(currPos, velocity):
    newPos = (currPos[X] + velocity[X], currPos[Y] + velocity[Y])
    if velocity[X] < 0:
        newXVel = velocity[X] + 1
    elif velocity[X] > 0:
        newXVel = velocity[X] - 1
    else:
        newXVel = 0
    newVelocity = (newXVel, velocity[Y]-1)
    return (newPos, newVelocity)


def checkInBound(point, xBound, yBound):
    xBoundList = list(xBound)
    xBoundList.sort()
    yBoundList = list(yBound)
    yBoundList.sort()
    result = OUTSIDE
    if(point[X] >= xBoundList[0] and point[X] <= xBoundList[1]):
        result |= ONLY_X
    if(point[Y] >= yBoundList[0] and point[Y] <= yBoundList[1]):
        result |= ONLY_Y
    return result


def checkLost(curPos, xBound, yBound):
    xBoundList = [abs(x) for x in xBound]
    xBoundList.sort()
    yBoundList = list(yBound)
    yBoundList.sort()
    if(abs(curPos[X]) > abs(xBoundList[1])):
        return True
    if(curPos[Y] < yBoundList[0]):
        return True
    return False


def printMap(curPos, targetBound, startPos, positions=None):
    for y in range(gMax[Y], gMin[Y]-1, -1):
        for x in range(gMin[X], gMax[X]+1):
            if((x, y) == curPos):
                print("X", end='')
            elif checkInBound((x, y), targetBound[0], targetBound[1]) == INSIDE:
                print("#", end='')
            elif ((x, y) == startPos):
                print('S', end='')
            elif positions is not None and (x, y) in positions:
                print("X", end='')
            else:
                print('.', end='')
        print('')
    print('')





def mainLogic(data):
    print(data)
    m = regexPatt.match(data)
    minX = int(m.group(1))
    maxX = int(m.group(2))
    minY = int(m.group(3))
    maxY = int(m.group(4))

    print(minX, maxX)
    print(minY, maxY)

    minTarget = (minX, minY)
    maxTarget = (maxX, maxY)

    recordBounds(minTarget)
    recordBounds(maxTarget)

    startPos = (0, 0)
    curPos = startPos
    curVel = (10, 247)

    printMap(curPos, ((minX, maxX), (minY, maxY)), startPos)
    positions = set()
    recordBounds(curPos)
    while True:
        curPos, curVel = computePos(curPos, curVel)
        recordBounds(curPos)
        status = checkInBound(curPos, (minX, maxX), (minY, maxY))
        positions.add(curPos)
        print(curPos, curVel)
        if INSIDE == status or checkLost(curPos, (minX, maxX), (minY, maxY)):
            if(INSIDE != status):
                print("out of bound")
            break
    
    #printMap(curPos, ((minX, maxX), (minY, maxY)), startPos, positions)


def readAndProcessInputs():

    parser = ArgumentParser()
    parser.add_argument("-file", "-f", default="sample.txt")
    parser.add_argument("-test", "-t", default=None)
    args = parser.parse_args()

    if(args.test is not None):
        return args.test

    with open(args.file) as f:
        lines = f.readlines()

    commands = [x.strip() for x in lines]
    return commands[0]


if __name__ == "__main__":
    data = readAndProcessInputs()
    mainLogic(data)
