from math import floor


def computeFuel(mass):
    fuel =floor(mass/3)-2
    return fuel
    

tot = 0

with open("input.txt") as f:
    lines = f.readlines()


for line in lines:
    mass = int(line.strip())
    
    fuel = computeFuel(mass)    
    tot +=fuel
    print("Mass {} - Fuel {} - Total {}".format(mass,fuel,tot))

print("Total fuel {}".format(tot))


