import subprocess
import os

NORTH = 1
SOUTH = 2
WEST = 3
EAST = 4

WALL = 0
MOVED = 1
MOVED_END = 2
OXYGEN = 3


DIR_KEY = {
    1 : 'NORTH',
    2 : 'SOUTH',
    3 : 'WEST',
    4 : 'EAST',
}

STATUS_MAP = {
    WALL : "\033[91m#\033[00m",
    MOVED : ".",
    MOVED_END: "*",
    OXYGEN: "\033[92mO\033[00m"
}

OPP = {
    NORTH : SOUTH,
    SOUTH : NORTH,
    EAST : WEST,
    WEST : EAST
}

computer = None

def start():
    global computer
    computer = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    

def comp_move(direction):
    computer.stdin.write(str(direction)+"\n")
    computer.stdin.flush()
    temp = computer.stdout.readline()
    return int(temp.strip())

def moved(coordinate,direction):
    if(direction == NORTH):
        return (coordinate[0],coordinate[1]+1)
    if(direction == SOUTH):
        return (coordinate[0],coordinate[1]-1)
    if(direction == WEST):
        return (coordinate[0]-1,coordinate[1])
    if(direction == EAST):
        return (coordinate[0]+1,coordinate[1])

def markLoc(bitmap,status,coordinate):
    if(coordinate[0] not in bitmap):
        bitmap[coordinate[0]] = {}
    bitmap[coordinate[0]][coordinate[1]] = status

def cache(coordinate,status,hall,oxygen):
    if(status == MOVED):
        hall.add(coordinate)
    elif(status == MOVED_END):
        oxygen.add(coordinate)

def getAdjacentHall(loc,oxygen,hall):
    halls = []
    for i in range(1,5):
        nextLoc = moved(loc,i)
        if nextLoc not in oxygen and nextLoc in hall:
            halls.append(nextLoc)
    return halls

def fillWithOxygen(bitMap,hall,oxygen):
    minute = 0

    while len(hall) >0:
        nextHalls = []
        for loc in oxygen:
            nextHalls.extend(getAdjacentHall(loc,oxygen,hall))
        
        for loc in nextHalls:
            oxygen.add(loc)
            hall.remove(loc)
            bitMap[loc[0]][loc[1]] = OXYGEN
        minute+=1
        printMap(bitMap)
    print("Minutes to complete {}".format(minute))


def move():
    global computer

    currentCoordinate = (0,0)
    bitMap = {}
    visited = set()
    hall = set()
    oxygen = set()
    visited.add(currentCoordinate)
    stack = []
    goal = None

    targetFlag = False 
    steps =0
    while True:
        travelled = False
        for direction in range(1,5):
            nextCoordinate = moved(currentCoordinate,direction)
            if(nextCoordinate in visited):
                continue
            status = comp_move(direction)
            markLoc(bitMap,status,nextCoordinate)
            cache(nextCoordinate,status,hall,oxygen)
            if(WALL==status):
                continue
            elif MOVED_END == status:
                targetFlag = True
            travelled = True
            break
        
        if(targetFlag):
            #print("Target Found in {} steps".format(steps))
            targetFlag = False
            if goal is None or goal[1] > steps:
                goal = (nextCoordinate,steps)
            #break

        if(travelled):
            stack.append(direction)
            currentCoordinate = nextCoordinate
            visited.add(currentCoordinate)
            #print("Moved to {}".format(currentCoordinate))
            steps+=1
        elif(len(stack)>0):
            back_dir = OPP[stack.pop()]
            status = comp_move(back_dir)
            currentCoordinate = moved(currentCoordinate,back_dir)
            #print("Went back to {}".format(currentCoordinate))
            steps-=1
        else:
            break

        printMap(bitMap)
    
    fillWithOxygen(bitMap,hall,oxygen)
    print("Target Found at {}, {} steps from start".format(goal[0],goal[1]))



def printMap(bitMap):
    os.system('cls')  # For Windows
    rows = bitMap.keys()
    rows.sort()
    #print("Min_row {} Max_row {}".format(rows[0],rows[-1]))
    min_row = rows[0]
    max_row = rows[-1]
    min_column = 1000
    max_column=-1000
    for row in rows:
        cols = bitMap[row].keys()
        cols.sort()
        if( cols[0] <min_column):
            min_column = cols[0]
        if(cols[-1] > max_column):
            max_column = cols[-1]

    #print("min_column {} max_column {}".format(min_column,max_column))

    for row in range(min_row,max_row+1):
        rowString = ""
        for col in range(min_column,max_column+1):
            if(row in bitMap and col in bitMap[row]):
                rowString+=STATUS_MAP[bitMap[row][col]]
            else:
                rowString+=" "
        print(rowString)



if __name__ == "__main__":
    start()

    move()

    #print(min_step)
    

    computer.terminate()
