
import time
def check(val):
    val =str(val)
    if(len(val)!=6): #redundant check
        #print(len(val))
        return False
    prev = ''
    double = False
    for char in val:
        #print(prev,char)
        if prev=='':
            prev = char
        else:
            if(prev>char):
                #print("{}>{}".format(prev,char))
                return False
            elif prev == char:
                #print("{}=={}".format(prev,char))
                double = True
            prev = char
    return double # whether double or not will be the final check


with open('input.txt') as f:
    input_line = f.readline()

input_line = input_line.strip().split("-")

lower = int(input_line[0])
upper =int(input_line[1])
t0 = time.time()
count = 0
for i in range(lower,upper):
    if(check(i)):
        count+=1
t1 = time.time()
print('{} - found in {}'.format(count,t1-t0))
        

#print(check(input()))