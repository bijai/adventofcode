
import time

def check(val):
    val =str(val)
    if(len(val)!=6): #redundant check
        #print(len(val))
        return False
    prev = ''
    double = False
    pdouble = False     #got a double already flag
    dchar = ''          #repeating character
    for char in val:
        #print(prev,char)
        if prev=='':
            prev = char
        else:
            if(prev>char): #increasing digits check
                #print("{}>{}".format(prev,char))
                return False
            elif prev == char:
                #print("{}=={}".format(prev,char))
                if(dchar==''):
                    double = True
                    dchar = char        #store the repeating digit to check for larger group
                elif(dchar == char):    #3 or more of repeating digit .. nop
                    double = False
            elif double:
                dchar = ''
                pdouble = True # flag that we have got a double digit already
                #need to continue iteration to check for increasing digits
            else:
                dchar = ''
            prev = char
    return double if not pdouble else True #having a double digit is the last validation


with open('input.txt') as f:
    input_line = f.readline()

input_line = input_line.strip().split("-")

lower = int(input_line[0])
upper =int(input_line[1])
t0 = time.time()
count = 0
for i in range(lower,upper):
    if(check(i)):
        count+=1
t1 = time.time()
print('{} - found in {}'.format(count,t1-t0))
        

#print(check(input()))