from itertools import combinations 

HOR = 1
VER = 2
X = 0
Y = 1

class Line:
    def __init__(self,notation=None,currentPos=None):
        self.x = None
        self.x1 = None
        self.x2 = None
        self.hx = None
        self.lx = None

        self.y = None
        self.y1 = None
        self.y2 = None
        self.hy = None
        self.ly = None

        if notation:
            
            dir = notation[0:1]
            length = int(notation[1:])
            if dir == 'R':
                self.x1 = currentPos[0]
                self.x2 = self.x1+length
                self.y = currentPos[1]

                if self.x1>self.x2:
                    self.hx = self.x1
                    self.lx = self.x2
                else:
                    self.hx = self.x2
                    self.lx = self.x1
            elif dir == 'L':
                self.x1 = currentPos[0]
                self.x2 = self.x1-length
                self.y = currentPos[1]
                if self.x1>self.x2:
                    self.hx = self.x1
                    self.lx = self.x2
                else:
                    self.hx = self.x2
                    self.lx = self.x1
            elif dir == 'U':
                self.y1 = currentPos[1]
                self.y2 = self.y1+length
                self.x = currentPos[0]
                if self.y1>self.y2:
                    self.hy = self.y1
                    self.ly = self.y2
                else:
                    self.hy = self.y2
                    self.ly = self.y1
            elif dir == 'D':
                self.y1 = currentPos[1]
                self.y2 = self.y1-length
                self.x = currentPos[0]
                if self.y1>self.y2:
                    self.hy = self.y1
                    self.ly = self.y2
                else:
                    self.hy = self.y2
                    self.ly = self.y1

    def getEnd(self):
        if self.x is not None:
            return (self.x,self.y2)
        if self.y is not None:
            return (self.x2,self.y)
        return ()
    def getStart(self):
        if self.x is not None:
            return (self.x,self.y1)
        if self.y is not None:
            return (self.x1,self.y)
        return ()
    
           
    def findIntersection(self,line):
        if(self.x is not None and line.y is not None):
            if(self.ly < line.y < self.hy) and (line.lx < self.x < line.hx):
                return ((self.x,line.y),(line.hx-self.x)+(self.hy-line.y))
            else:
                return None
        if(self.y is not None and line.x is not None):
            if(self.lx < line.x < self.hx) and (line.ly < self.y < line.hy):
                return ((line.x,self.y),(line.hy-self.y)+(self.hx-line.x))
            else:
                return None
        return None

    def __repr__(self):
        return ("{},"*10).format(self.x,self.x1,self.x2,self.hx,self.lx,self.y,self.y1,self.y2,self.hy,self.ly)

    def length(self):
        if self.x is not None:
            return self.hy-self.ly
        if self.y is not None:
            return self.hx-self.lx
    
def ManDistOrigin(point):
    return abs(point[0])+abs(point[1])
    

        

with open("input.txt") as f:
    fileLines = f.readlines()


pLines = []
for fline in fileLines:
    
    lines = []
    operations = fline.strip().split(',')
    start = (0,0)
    for op in operations:
        lines.append(Line(op,start))
        start = lines[len(lines)-1].getEnd()
        #print (lines[len(lines)-1])
    pLines.append(lines)

intersections = []
#print(pLines)

for l1,l2 in combinations(range(len(pLines)),2):
    line1 = pLines[l1]
    line2 = pLines[l2]
    steps1 = 0
    for sl1 in line1:
        steps2 = 0
        for sl2 in line2:
            
            i = sl1.findIntersection(sl2)
            if i :
                intersections.append((i[0],steps1+steps2+i[1]))
                #print("{} - {} vs {} - {} at {} - steps {}:{}:{}".format(sl1.getStart(),sl1.getEnd(),sl2.getStart(),sl2.getEnd(),i,(steps1,steps2),i[1],steps1+steps2+i[1]))
            #else:
                #print("{} - {} vs {} - {} - steps {}".format(sl1.getStart(),sl1.getEnd(),sl2.getStart(),sl2.getEnd(),(steps1,steps2)))
            steps2 += sl2.length()
        steps1 += sl1.length()


mini = None
miniSteps = None
for intersection in intersections:
    dist = ManDistOrigin(intersection[0])
    if(mini is None or dist < mini):
        mini = dist
    if(miniSteps is None or intersection[1] < miniSteps):
        miniSteps = intersection[1]
    print("{:<15} : Steps[{:<7}]  Distance[{:<5}]".format(intersection[0],intersection[1],dist))


print("Shortest distance : {}".format(mini))
print("Shortest steps    : {}".format(miniSteps))



        
            




