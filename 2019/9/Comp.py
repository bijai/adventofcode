import sys
#CONSTANTS
ADD = 1
MUL = 2
INP = 3
OUT = 4
JIT = 5
JIF = 6
LT = 7
EQ = 8
RBA = 9
END = 99

OPCODE = {
1 : 'ADD',
2 : 'MUL',
3 : 'INP',
4 : 'OUT',
5 : 'JIT',
6 : 'JIF',
7 : 'LT ',
8 : 'EQ ',
9 : 'RBA'}

POS_MODE = 0
IMM_MODE = 1
REL_MODE = 2


DEBUG = False

ArgLength = { 
    ADD : 3,
    MUL : 3,
    LT : 3,
    EQ : 3,
    INP : 1,
    OUT : 1,
    JIT : 2,
    JIF : 2,
    RBA : 1
}

AddMode = [INP]

def getArgLength(op):
    return ArgLength[op]

def readMem(program,memory,address):
    if(address < len(program)):
        return int(program[address])
    if(address not in memory):
        memory[address] = 0
    return memory[address]

def writeMem(program,memory,address,data):
    if(address < len(program)):
        program[address] = data
    else:
        memory[address] = data
        

def execute(program):
    rel_base = 0
    inputIdx= 0
    index = 0
    memory = {}
    while(readMem(program,memory,index)!=END):
        temp =  readMem(program,memory,index)
        op = temp%100
        mode = []
        temp = int(temp/100)
        while (temp>0):
            mode.append(temp%10)
            temp=int(temp/10)

        arg = []
        oArgs = []
        reqArgs = getArgLength(op)
        
        for i in range(len(mode)): # i is also the argument number
            if(mode[i]==POS_MODE):
                pos = readMem(program,memory,index+i+1)
                oArgs.append(pos)
                arg.append(readMem(program,memory,pos))
            elif mode[i]==IMM_MODE:
                oArgs.append(None)
                arg.append(readMem(program,memory,index+i+1))
            elif mode[i]==REL_MODE:
                pos = readMem(program,memory,index+i+1)
                if(op in AddMode or (reqArgs==3 and i==reqArgs-1)):
                    oArgs.append(None)
                    arg.append(rel_base+pos)
                else:
                    oArgs.append(rel_base+pos)
                    arg.append(readMem(program,memory,rel_base+pos))
        

        i = len(arg) 
        while len(arg) < reqArgs-1:
            pos = readMem(program,memory,index+i+1)
            oArgs.append(pos)
            arg.append(readMem(program,memory,pos))
            i+=1
        
        if(len(arg)==reqArgs-1): # last argument
            if( op == JIT or op == JIF or op == OUT or op == RBA):
                pos = readMem(program,memory,index+i+1)
                oArgs.append(pos)
                arg.append(readMem(program,memory,pos))
            else:
                oArgs.append(None)
                arg.append(readMem(program,memory,index+i+1))
            i+=1

        output = ''
        jumpTo = None
        if(op==ADD):
            writeMem(program,memory,arg[2],arg[0]+arg[1])
            output = readMem(program,memory,arg[2])
        elif(op==MUL):
            writeMem(program,memory,arg[2],arg[0]*arg[1])
            output = readMem(program,memory,arg[2])
        elif(op==INP):
            writeMem(program,memory,arg[0],int(input("Input")))
            inputIdx+=1
            
            output = readMem(program,memory,arg[0])
        elif(op==OUT):
            print("Output : {}".format(arg[0]))
            sys.stdout.flush()
        elif(op==LT):
            if(arg[0] < arg[1]):
                writeMem(program,memory,arg[2],1)
            else : writeMem(program,memory,arg[2],0)
            output = readMem(program,memory,arg[2])
        elif(op==EQ):
            if(arg[0] == arg[1]):
                writeMem(program,memory,arg[2],1)
            else : writeMem(program,memory,arg[2],0)
            output = readMem(program,memory,arg[2])
        elif(op==JIT):
            if(arg[0] != 0):
                jumpTo = arg[1]
            output = jumpTo
        elif(op==JIF):
            if(arg[0] == 0):
                jumpTo = arg[1]
            output = jumpTo
        elif(op==RBA):
            rel_base+=arg[0]
            output = rel_base
        else:
            raise Exception("Invalid op {}".format(op))
        
        if(DEBUG):
            print("{:<5}{:<4}:{:<4}A:{:<30}oA:{:<20}M:{:<12}O:{:<10}".format(program[index],index,OPCODE[op],str(arg),str(oArgs),str(mode),str(output)))

        if jumpTo is None:
            index += (reqArgs+1)
        else: 
            index = jumpTo

    return program
            
if __name__ == "__main__":

    with open("9\\input.txt") as f:
        program = f.readline()

    program = str(program).strip()

    if DEBUG:
        print (program)
    
    program = program.split(',')

    execute(program[:])