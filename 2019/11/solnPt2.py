import subprocess
from PIL import Image

UP = 0
RIGHT = 90
DOWN = 180
LEFT = 270

BLACK = 0
WHITE = 1

LEFTTURN = 0
RIGHTTURN = 1




def start():
    computer = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    return computer
                            


def addPanel(xy,color=None):
    global panelSet,panels
    if(xy[0] not in panels):
        panels[xy[0]] = {}
    panels[xy[0]][xy[1]] = color
    panelSet.add(xy)

def getColor(x,y):
    global panels
    if(x not in panels):
        return BLACK
    if(y not in panels[x]):
        return BLACK
    return panels[x][y]


def compMove(current,currentDir,rotate):
    if(rotate == LEFTTURN):
        direction = (currentDir-90)%360
    elif(rotate == RIGHTTURN):
        direction = (currentDir+90)%360

    if direction == UP:
        return ((current[0],current[1]+1),direction)
    elif direction == DOWN:
        return ((current[0],current[1]-1),direction)
    elif direction == RIGHT:
        return ((current[0]+1,current[1]),direction)
    elif direction == LEFT:
        return ((current[0]-1,current[1]),direction)
    else:
        raise Exception("What direction is {}".format(direction))



def execute():
    global panelSet,panels

    panelSet = set()
    panels = {}

    on = (0,0)
    color = WHITE
    addPanel(on,color)
    direction = 0

    comp = start()
    while True:
        if comp.poll() != None:
            print("Intcode stopped")
            break
        comp.stdin.write(str(color)+"\n")
        temp = comp.stdout.readline().strip("\r\n")
        if(len(temp)==0):
            print("no output")
            break
        paint = int(temp)
        temp = comp.stdout.readline().strip("\r\n")
        if(len(temp)==0):
            print("no output")
            break
        rotate = int(temp)
        addPanel(on,paint)
        print("Pos[{}] P[{}] M[{}]".format(on,paint,rotate))
        on,direction = compMove(on,direction,rotate)
        color = getColor(on[0],on[1])


    comp.terminate()

    min_x = sorted(panels.keys())[0]
    max_x = sorted(panels.keys())[-1]

    min_y = sorted(panels[0].keys())[0]
    max_y = sorted(panels[0].keys())[-1]

    for i in panels:
        t_min_y = sorted(panels[i].keys())[0]
        t_max_y = sorted(panels[i].keys())[-1]
        if(t_min_y<min_y):
            min_y = t_min_y
        if(t_max_y>max_y):
            max_y = t_max_y
        
    print(min_x,max_x,min_y,max_y)

    
    image = createImage(max_x-min_x+1,max_y-min_y+1)
    for i in panels:
        for j in panels[i]:
            mj = abs(j)
            if(panels[i][j] == BLACK):
                image.putpixel((i,mj),(0,0,0,255))
            elif(panels[i][j] == WHITE):
                image.putpixel((i,mj),(255,255,255,255))

    image.save("output.png")    


def createImage(XSize,YSize):
    img = Image.new('RGBA', (XSize, YSize), color = (128,128,128,0))  
    return img




if __name__ == "__main__":
    execute()