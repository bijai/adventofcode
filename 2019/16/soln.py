

def readFile(file):
    with open(file) as f:
        line = f.readline().strip()

    return line


def explodeList(list1,list2):
    if(list1 is None):
        return list2
    list1.extend(list2)
    return list1

def getPattern(number,length):
    result = []
    BASE = [1,0,-1,0]
    while(len(result) <length):
        temp = [[x] * number for x in BASE]
        temp = reduce(explodeList,temp)
        result.extend(temp)
    
    return result[:length]
        

def multiply(plist,pattern):
    result = 0
    for idx,elem in enumerate(plist):
        result+= (elem*pattern[idx])

    return abs(result)%10

def strToList(p_str):
    return [int(char) for char in p_str]


if __name__ == "__main__":
    line = readFile("input.txt")
    num = len(line)
    
    phases = 100
    i_line = strToList(line)
    for phase in range(phases):
        r_line = []
        for i in range(num):
            r_line.append(multiply(i_line[i:],getPattern(i+1,num)))
        i_line = r_line[:]
    
    print(r_line[:8])
        




    