import time

def readFile(file):
    with open(file) as f:
        line = f.readline().strip()

    return line


def explodeList(list1,list2):
    if(list1 is None):
        return list2
    list1.extend(list2)
    return list1

def getPattern(number,length):
    result = []
    BASE = [1,0,-1,0]
    while(len(result) <length):
        temp = [[x] * number for x in BASE]
        for l in temp:
            result.extend(l)
            if len(result) > length:
                return result[:length]
    return result[:length]
        

def multiply(plist,number):

    BASE = [1,0,-1,0]
    result = 0
    idx = 0
    pidx =0
    pLen = len(plist)
    while(idx <pLen):
        m= BASE[pidx]
        if(m == 0):
            idx+=number
        else:
            t_len = number if (idx+number)<pLen else pLen-idx
            if(m == 1):
                result += reduce(lambda a,b: a+b,plist[idx:idx+t_len])
            else:
                result -= reduce(lambda a,b: a+b,plist[idx:idx+t_len])
            idx+=t_len
        pidx+=1
        pidx%=4
        

    return abs(result)%10

def strToList(p_str):
    return [int(char) for char in p_str]


if __name__ == "__main__":
    line = readFile("test.txt")
    
    phases = 100
    i_line = strToList(line) * 10000
    num = len(line) * 10000
    for phase in range(phases):
        r_line = []
        start = time.time()
        for i in range(num):
            r_line.append(multiply(i_line[i:],i+1))
            if(i%100 == 0):
                end = time.time()
                print(phase,i,end - start)
                start = time.time()
        i_line = r_line[:]
    
    
    print(r_line[int(line[:7]):8])
    #print(r_line[:8])
        




    