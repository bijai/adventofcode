import time

def readFile(file):
    with open(file) as f:
        line = f.readline().strip()

    return line


def explodeList(list1,list2):
    if(list1 is None):
        return list2
    list1.extend(list2)
    return list1


        

def multiply(plist,number):
    return (reduce(lambda a,b: a+b,plist))%10
    

def strToList(p_str):
    return [int(char) for char in p_str]


if __name__ == "__main__":
    line = readFile("input.txt")
    
    phases = 100
    i_line = strToList(line) * 10000
    start_idx = int("".join(line[:7]))
    i_line = i_line[start_idx:]
    num = len(i_line) 
    
    for phase in range(phases):
        start = time.time()
        r_line = []
        for i in range(num-1,-1,-1):
            if(len(r_line) ==0):
                r_line.append(i_line[i])
            else:
                r_line.append(r_line[-1]+i_line[i])
        r_line.reverse()
        i_line = r_line
        end = time.time()
        print(phase,end-start)

            
            
    
    
    #print(r_line[int(line[:7]):8])
    print("".join([str(x%10) for x in r_line[:8]]))
        




    