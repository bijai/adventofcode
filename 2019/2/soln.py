
#CONSTANTS
ADD = 1
MUL = 2
END = "99"
DEBUG = False

def execute(program,a,b):
    #initialize inputs
    program[1]=a
    program[2]=b

    index = 0
    while(index<len(program) and program[index]!=END):
        op = int(program[index])
        pos1 = int(program[index+1])
        pos2 = int(program[index+2])

        arg1 = int(program[pos1])
        arg2 = int(program[pos2])
        opPos = int(program[index+3])

        if(op==ADD):
            program[opPos] = arg1+arg2
        elif(op==MUL):
            program[opPos] = arg1*arg2
        
        if(DEBUG):
            print("{} {} {} {} : {} {} : {}".format(op,pos1,pos2,opPos,arg1,arg2, program[opPos]))

        index+=4
    return program

def findInputs(program,output):
    #brute force
    for i in range(100):
        for j in range(100):
            tprogram = program[:]
            execute(tprogram,i,j)
            if(DEBUG):
                print(i,j,tprogram[0])
            if(tprogram[0] == output):
                return (i,j)
    return (-1,-1)


            
if __name__ == "__main__":

    with open("input.txt") as f:
        program = f.readline()

    program = str(program).strip()

    if DEBUG:
        print (program)
    
    program = program.split(',')

    print("Part 1")
    print("Input [{},{}] : Output [{}]".format(12,2,execute(program[:],12,2)[0]))

    print("Part 2")
    i,j = findInputs(program,19690720)

    if(-1,-1) == (i,j):
        print("Inputs not found")
    else :
        print ("Noun [{}], Verb [{}], Answer [{}]".format(i,j,i*100+j))


