import re
import math

PATTERN = re.compile(r"(.*) => (\d+) (\w+)")
RPATTERN = re.compile(r"(\d+)\s(\w+)")

leftover = {}

class Element:
    def __init__(self,name,qty):
        self.name = name.strip()
        self.qty = int(qty.strip())

    def __repr__(self):
        return "{} {}".format(self.qty,self.name)

    def __hash__(self):
        return hash(self.name)

class Reaction:
    def __init__(self, reactionLine):
        self.result = None
        self.rawMaterials = []
        self.parseLine(reactionLine)
    
    def parseLine(self, line):
        m = PATTERN.match(line.strip())
        if(m):
            #print("{} makes {}{}".format(m.group(1),m.group(2),m.group(3)))
            self.result = Element(m.group(3),m.group(2))
            rawMat = m.group(1)
            match = RPATTERN.findall(rawMat)
            for m in match:
                self.rawMaterials.append(Element(m[1],m[0]))

        else:
            raise Exception("Invalid pattern {}".format(line))

    def __repr__(self):
        return "{} = {}".format(self.result," ".join([ str(x) for x in self.rawMaterials]))


def readFile(file):
    with open(file) as f:
        lines = f.readlines()
    
    reactions = {}
    for line in lines:
        r = Reaction(line)
        reactions[r.result.name] = r
        leftover[r.result.name] = 0

    return reactions



def findQty(reactions,elem,qty):
    global leftover
    if(elem == "ORE"):
        return qty

    reaction = reactions[elem]
    req = 1

    if(leftover[elem] > 0):
        if(leftover[elem] >= qty):
            leftover[elem]-=qty
            return 0
        else:
            qty-= leftover[elem]
            leftover[elem]=0
        
    if(reaction.result.qty<qty):
        req = math.ceil(qty/reaction.result.qty)

    total = 0
    for raw in reaction.rawMaterials:
        total += (findQty(reactions,raw.name,req*raw.qty))

    leftover[elem] += (req*reaction.result.qty)-qty
    return total
        
def resetLeftOvers():
    global leftover
    for elem in leftover:
        leftover[elem] = 0


if __name__ == "__main__":
    reactions = readFile("input.txt")

    perFuel = findQty(reactions,"FUEL",1)
    print("ores for 1 Fuel :{}".format(perFuel))

    oresInHand = 1000000000000

    fuelGenerated = oresInHand//perFuel


    print("Initial {}".format(fuelGenerated))
    oresReq = 0
    while(oresReq < oresInHand):
        resetLeftOvers()
        oresReq = findQty(reactions,"FUEL",fuelGenerated)

        balance = oresInHand-oresReq
        fuelFromBal = balance//perFuel
        print(oresReq,fuelGenerated,fuelFromBal)
        if fuelFromBal == 0:
            break
        fuelGenerated += fuelFromBal

    print(oresReq,fuelGenerated)



    