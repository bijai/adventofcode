import subprocess

def start(params):
    global A,B,C,D,E

    A = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
                            
    B = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    C = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    D = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    E = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )
    A.stdin.write(str(params[0])+"\n")
    B.stdin.write(str(params[1])+"\n")
    C.stdin.write(str(params[2])+"\n")
    D.stdin.write(str(params[3])+"\n")
    E.stdin.write(str(params[4])+"\n")


def execute(params):
    global A,B,C,D,E
    start(params)
    A.stdin.write("0\n")
    working = True
    while working:
        try:
            temp = A.stdout.readline().strip("\r\n")
            if(len(temp)==0):
                break
            #print("A",temp)
            B.stdin.write(temp+"\n")
            temp = B.stdout.readline().strip("\r\n")
            #print("B",temp)
            C.stdin.write(temp+"\n")
            temp = C.stdout.readline().strip("\r\n")
            #print("C",temp)
            D.stdin.write(temp+"\n")
            temp = D.stdout.readline().strip("\r\n")
            #print("D",temp)
            E.stdin.write(temp+"\n")
            oute = E.stdout.readline().strip("\r\n")
            #print("E",oute)
            if E.poll() != None:
                working=False
            else:
                A.stdin.write(oute+"\n")
        except: 
            print("Exception")
            working = False

    A.terminate()
    B.terminate()
    C.terminate()
    D.terminate()
    E.terminate()
    return int(oute)





high = 0
params = []
allp = [5,6,7,8,9]
for a in allp:
    BParams = [x for x in allp if x != a]
    for b in BParams:
        CParams = [x for x in BParams if x != b]
        for c in CParams:
            DParams = [x for x in CParams if x != c]
            for d in DParams:
                EParams = [x for x in DParams if x != d]
                for e in EParams:
                    opE = execute([a,b,c,d,e])
                    if(opE > high):
                        high =opE
                        params = [a,b,c,d,e]
                    print([a,b,c,d,e],opE)

print (high,params)