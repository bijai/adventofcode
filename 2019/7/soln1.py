
#CONSTANTS
ADD = 1
MUL = 2
INP = 3
OUT = 4
JIT = 5
JIF = 6
LT = 7
EQ = 8

OPCODE = {
1 : 'ADD',
2 : 'MUL',
3 : 'INP',
4 : 'OUT',
5 : 'JIT',
6 : 'JIF',
7 : 'LT ',
8 : 'EQ '}

POS_MODE = 0
IMM_MODE = 1

END = "99"
DEBUG = False

ArgLength = { 
    ADD : 3,
    MUL : 3,
    LT : 3,
    EQ : 3,
    INP : 1,
    OUT : 1,
    JIT : 2,
    JIF : 2,
}

def getArgLength(op):
    return ArgLength[op]


def execute(program,inputs):
    
    inputIdx= 0
    outputs = []
    index = 0
    while(index<len(program) and program[index]!=END):
        temp =  int(program[index])
        op = temp%100
        mode = []
        temp = int(temp/100)
        while (temp>0):
            mode.append(temp%10)
            temp=int(temp/10)

        arg = []
        oArgs = []
        for i in range(len(mode)): # i is also the argument number
            if(mode[i]==POS_MODE):
                pos = int(program[index+i+1])
                oArgs.append(pos)
                arg.append(int(program[pos]))
            elif mode[i]==IMM_MODE:
                oArgs.append(-1)
                arg.append(int(program[index+i+1]))
        reqArgs = getArgLength(op)

        i = len(arg) 
        while len(arg) < reqArgs-1:
            pos = int(program[index+i+1])
            oArgs.append(pos)
            arg.append(int(program[pos]))
            i+=1
        
        if(len(arg)==reqArgs-1): # last argument
            if( op == JIT or op == JIF or op == OUT):
                pos = int(program[index+i+1])
                oArgs.append(pos)
                arg.append(int(program[pos]))
            else:
                oArgs.append(-1)
                arg.append(int(program[index+i+1]))
            i+=1

        output = ''
        jumpTo = None
        if(op==ADD):
            program[arg[2]] = arg[0]+arg[1]
            output = program[arg[2]]
        elif(op==MUL):
            program[arg[2]] = arg[0]*arg[1]
            output = program[arg[2]]
        elif(op==INP):
            #program[arg[0]] = int(input("Input : "))
            program[arg[0]] = int(inputs[inputIdx])
            inputIdx+=1
            
            output = program[arg[0]]
        elif(op==OUT):
            #print("OUTPUT : {}".format(arg[0]))
            outputs.append(arg[0])
        elif(op==LT):
            if(arg[0] < arg[1]):
                program[arg[2]] = 1
            else : program[arg[2]] = 0
            output = program[arg[2]]
        elif(op==EQ):
            if(arg[0] == arg[1]):
                program[arg[2]] = 1
            else : program[arg[2]] = 0
            output = program[arg[2]]
        elif(op==JIT):
            if(arg[0] != 0):
                jumpTo = arg[1]
            output = jumpTo
        elif(op==JIF):
            if(arg[0] == 0):
                jumpTo = arg[1]
            output = jumpTo
        else:
            raise Exception("Invalid op {}".format(op))
        
        if(DEBUG):
            print("{:<4}:{:<4}A:{:<30}oA:{:<20}M:{:<12}O:{:<10}".format(index,OPCODE[op],arg,oArgs,mode,output))

        if not jumpTo:
            index += (reqArgs+1)
        else: 
            index = jumpTo

    return outputs
            
if __name__ == "__main__":

    with open("input.txt") as f:
        program = f.readline()

    program = str(program).strip()

    if DEBUG:
        print (program)
    
    program = program.split(',')

    
    high = 0
    params = []
    allp = [0,1,2,3,4]
    for a in range(5):
        opA = execute(program[:],[a,0])
        BParams = [x for x in allp if x != a]
        for b in BParams:
            opB = execute(program[:],[b,opA[0]])
            CParams = [x for x in BParams if x != b]
            for c in CParams:
                opC = execute(program[:],[c,opB[0]])
                DParams = [x for x in CParams if x != c]
                for d in DParams:
                    opD = execute(program[:],[d,opC[0]])
                    EParams = [x for x in DParams if x != d]
                    for e in EParams:
                        opE = execute(program[:],[e,opD[0]])
                        if(opE[0] > high):
                            high =opE[0]
                            params = [a,b,c,d,e]
                        
    
    print (high,params)
'''
    testParams = [1,0,4,3,2]

    opA = execute(program[:],[testParams[0],0])
    opB = execute(program[:],[testParams[1],opA[0]])
    opC = execute(program[:],[testParams[2],opB[0]])
    opD = execute(program[:],[testParams[3],opC[0]])
    opE = execute(program[:],[testParams[4],opD[0]])

    print(opE[0])
'''

