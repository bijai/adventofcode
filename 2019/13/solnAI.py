import pygame
import subprocess
from threading import Thread
from time import sleep

gameDisplay = None

def initialize():
    global gameDisplay
    pygame.init()

    gameDisplay = pygame.display.set_mode((900,460))
    pygame.display.set_caption('Advent of Code : Day 13')



NONE = '0' 
WALL = '1' 
BLOCK = '2' 
PAD = '3' 
BALL = '4' 

black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
green = (0,255,0)
blue = (0,255,255)

LEFT = "-1"
RIGHT = "1"
STAY = "0"

colors = { 
    NONE:black,
    WALL:white,
    BLOCK:green,
    PAD:blue,
    BALL:red
            }


ball_x = 0
pad_x =0
changed = False

def start():
    computer = subprocess.Popen(['python', 'Comp.py'], 
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,text=True
                            )
    return computer
                            


comp = None
screen = {}

def getPixel(x,y):
    global screen
    if(x in screen):
        if(y in screen[x]):
            return screen[x][y]
    return NONE

def getColor(tile):
    global colors
    return colors[tile]

def draw(x,y,tile):
    global gameDisplay
    pygame.draw.rect(gameDisplay, getColor(tile), [x*20, y*20, 20, 20])

def putPixel(x,y,val):
    global screen,pad_x,ball_x,changed
    if x not in screen:
        screen[x] = {}
    screen[x][y] = val
    draw(x,y,val)
    if(val == PAD):
        pad_x = x
    elif(val == BALL):
        ball_x = x
        changed = True

def sendInput(val):
    global comp
    comp.stdin.write(val+"\n")
    comp.stdin.flush()

    
def execute():
    global screen,comp
    max_x = 0
    max_y = 0
    count = 0
    comp = start()
    while True:
    
        x = comp.stdout.readline().strip("\r\n")
        if(len(x)==0):
            print("no output")
            break

        y = comp.stdout.readline().strip("\r\n")
        tile = comp.stdout.readline().strip("\r\n")

        x = int(x)
        y = int (y)
        if(x > max_x):
            max_x = x
        if(y > max_y):
            max_y = y
        #print(x,y,tile)
        if( x== -1 and y == 0):
            print('SCORE {}'.format(tile))
        else:
            putPixel(x,y,tile)
        count+=1
        pygame.display.update()
    

    comp.terminate()

    print(max_x,max_y,count)
    Bcount = 0
    for i in range(max_x+1):
        for j in range(max_y+1):
            if(getPixel(i,j) == BLOCK):
                Bcount+=1
    print(Bcount)


def AI():
    global pad_x,ball_x,changed
    #print("AI : {} {} ".format(pad_x,ball_x))
    if changed:
        if(pad_x>ball_x):
            sendInput(LEFT)
        elif pad_x<ball_x:
            sendInput(RIGHT)
        else:
            sendInput(STAY)
        changed = False
        



if __name__ == "__main__":
    initialize()
    t = Thread(target=execute, daemon=True)
    t.start()
    sleep(3) #sleep for init
    clock = pygame.time.Clock()
    crashed = False
    downPressed = False
    keyPressed = True
    keyPressed = False
    count = 0
    while not crashed:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                crashed = True

        AI()
        pygame.display.update()
        clock.tick(120)
    t.join()
    pygame.quit()
    quit()
