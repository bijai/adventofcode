import argparse
import math
from fractions import Fraction as frac
from time import sleep
from os import system, name 

# define our clear function 
def clear(): 
  
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 
  

ASTROID_CHAR = '#'

class Astroid:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.visibleAstroids = set()
        self.hiddenAstroids = set()

    def __repr__(self):
        return "({},{})".format(self.x,self.y)

    def __hash__(self):
        return hash(str(self))

class Line:
    def __init__(self,x1,y1,x2,y2):
        if(x1!=x2):
            self.m = frac((y2-y1),(x2-x1))
            self.c = y1 - self.m*x1
            self.angle = math.atan2(self.m.numerator,self.m.denominator)
        else:
            self.m = None
            self.c = x1
            self.angle = math.radians(90)

    def inLine(self,x,y):
        if(self.m is None): #vertical line
            return x == self.c
        return (y == self.m*x+self.c)
    

def distance(x1,y1,x2,y2):
    direction = 1
    if(x1==x2 and y1>y2): #horizontal line
        direction = -1
    if(x1>x2):
        direction = -1
    absDist = math.sqrt(((x2-x1)**2) + ((y2-y1)**2))
    return direction*absDist



def readAstroidsFromFile(p_filePath):
    l_astroidDict = {}
    l_astroidList = []

    with open(p_filePath) as f:
        x=0
        y=0
        line = f.readline()
        while(line):
            line = line.strip()
            for char in line:
                if char==ASTROID_CHAR:
                    if(x not in l_astroidDict):
                        l_astroidDict[x]={}
                    l_astroidDict[x][y] = Astroid(x,y)
                    l_astroidList.append(l_astroidDict[x][y])
                x+=1
            max_x=x
            y+=1
            x=0
            line = f.readline()
        max_y=y
    return l_astroidList,l_astroidDict,max_x,max_y




def findHorAstroids(astroid,p_astroidDict):
    x = astroid.x
    y = astroid.y
    l_horAstroids = []
    pos_a_min =0,None
    neg_a_min =0,None
    for i in p_astroidDict:
        if x!=i and y in p_astroidDict[i]:
            l_horAstroids.append(p_astroidDict[i][y])

    if(len(l_horAstroids) == 1 ):
        l_horAstroids[0].visibleAstroids.add(astroid)
        astroid.visibleAstroids.add(l_horAstroids[0])
    elif len(l_horAstroids > 1):
        for t_a in l_horAstroids:
            if(t_a.x < x and (neg_a_min[1] is None or neg_a_min[0]<t_a.x )):
                neg_a_min= (t_a.x,t_a)
            elif(t_a.x > x and (pos_a_min[1] is None or pos_a_min[0]>t_a.x )):
                pos_a_min= (t_a.x,t_a)
        for t_a in l_horAstroids:
            if(t_a == pos_a_min[1] or t_a == neg_a_min[1]):
                t_a.visibleAstroids.add(astroid)
                astroid.visibleAstroids.add(t_a)
            else:
                t_a.hiddenAstroids.add(astroid)
                astroid.hiddenAstroids.add(t_a)

    print(astroid,l_horAstroids)
            
    



def computeVisibleAstroids(p_astroidList,x,y):

    for astroid in p_astroidList:
        astroid.hiddenAstroids.clear()
        astroid.visibleAstroids.clear()

    for astroid1 in p_astroidList:
        count = 0
        for astroid2 in p_astroidList:
            if astroid1==astroid2:
                continue
            if(astroid2 in astroid1.hiddenAstroids or astroid2 in astroid1.visibleAstroids):
               continue
            count+=1
            l_line = findLine(astroid1,astroid2)
            astroidsInLine = findAstroidsInLine(l_line,p_astroidList)
            distances = findDistances(astroid1,astroidsInLine)
            min_neg = -999
            neg_astroid = None
            min_pos = 999
            pos_astroid = None
            for astroid,dist in distances:
                if(dist<0):
                    if(min_neg < dist):
                        min_neg = dist
                        neg_astroid = astroid
                else:
                    if(min_pos > dist):
                        min_pos = dist
                        pos_astroid = astroid
            
            if(min_neg != -999):
                astroid1.visibleAstroids.add(neg_astroid)
                neg_astroid.visibleAstroids.add(astroid1)
            if(min_pos != 999):
                astroid1.visibleAstroids.add(pos_astroid)
                pos_astroid.visibleAstroids.add(astroid1)
            for astroid,dist in distances:
                if(min_neg != dist and min_pos !=dist):
                    astroid1.hiddenAstroids.add(astroid)
                    astroid.hiddenAstroids.add(astroid1)
            #print("{} - {} : {} {})".format(astroid1,astroid2,len(astroidsInLine),distances))
        print("{} : {}".format(astroid1,count))
                    

            


def findDistances(p_astroid,p_astroidList):
    distances = []
    for astroid in p_astroidList:
        if(astroid == p_astroid):
            continue
        distances.append((astroid,distance(p_astroid.x,p_astroid.y,astroid.x,astroid.y)))
    return distances


def findLine(astroid1,astroid2):
    return Line(astroid1.x,astroid1.y,astroid2.x,astroid2.y)

def findLineAsOrigin(astroid1,astroid2):
    return Line(0,0,astroid2.x-astroid1.x,astroid2.y-astroid1.y)

def findAstroidsInLine(line,p_astroidList):
    astroids = []
    for astroid in p_astroidList:
        if(line.inLine(astroid.x,astroid.y)):
            astroids.append(astroid)
    return astroids

def printWithList(p_astroidList,x,y):
    idx=0
    for i in range(y):
        temp_s = ''
        for j in range(x):
            if len(p_astroidList)>idx and p_astroidList[idx].x == j and p_astroidList[idx].y == i :
                temp_s+='#'
                idx+=1
            else:
                temp_s+='.'
        print(temp_s)

def printWithDict(p_astroidDict,x,y):
    for i in range(x):
        temp_s = ''
        for j in range(y):
            if j in p_astroidDict and i in p_astroidDict[j]:
                temp_s+='#'
            else:
                temp_s+='.'
        print(temp_s)


def destroyAllVisibleAstroids(p_astroidList,p_astroid):
    for astroid in p_astroid.visibleAstroids:
        if(astroid in p_astroidList):
            p_astroidList.remove(astroid)


def getAnglesToAstroids(p_astroid,p_astroidList):
    angles = []
    for astroid in p_astroidList:
        angles.append((astroid,findLineAsOrigin(p_astroid,astroid).angle))
    return angles


def main(p_inputFile):
    l_astroidList,l_astroidDict,max_x,max_y = readAstroidsFromFile(p_inputFile)
    print(max_x,max_y)
    
    #printWithDict(l_astroidDict,max_x,max_y)
    printWithList(l_astroidList,max_x,max_y)

    computeVisibleAstroids(l_astroidList,max_x,max_y)
    l_max = 0
    IMS = None
    for astroid in l_astroidList:
        #print("{} : H:[{}] V:[{}] {}".format(astroid,len(astroid.hiddenAstroids),len(astroid.visibleAstroids),len(astroid.hiddenAstroids)+len(astroid.visibleAstroids)))
        if(len(astroid.visibleAstroids) > l_max):
            l_max = len(astroid.visibleAstroids)
            IMS = astroid
    print(l_max)
    print(IMS)

    dCount = 200

    while(dCount - len(IMS.visibleAstroids)) > 0:
        dCount -= len(IMS.visibleAstroids)
        destroyAllVisibleAstroids(l_astroidList,IMS)
        computeVisibleAstroids(l_astroidList,max_x,max_y)
        print("Astroids left:{}, Destroy Count:{}".format(len(l_astroidList),dCount))
        printWithList(l_astroidList,max_x,max_y)

    topAstroid = None
    bottomAstroid = None

    for astroid in IMS.visibleAstroids:
        if(astroid.x==IMS.x):
            if(astroid.y > IMS.y):
                bottomAstroid = astroid
            else:
                topAstroid = astroid
    
    rightAstroids = [a for a in IMS.visibleAstroids if a.x > IMS.x]
    leftAstroids = [a for a in IMS.visibleAstroids if a.x < IMS.x]

    orderedAstroids = []
    if(topAstroid is not None):
        orderedAstroids.append((topAstroid,findLineAsOrigin(IMS,topAstroid).angle))
    
    rightAstroids = getAnglesToAstroids(IMS,rightAstroids)
    rightAstroids.sort(key=lambda score: score[1])
    orderedAstroids.extend(rightAstroids)

    if(bottomAstroid is not None):
        orderedAstroids.append((bottomAstroid,findLineAsOrigin(IMS,bottomAstroid).angle))

    leftAstroids = getAnglesToAstroids(IMS,leftAstroids)
    leftAstroids.sort(key=lambda score: score[1])
    orderedAstroids.extend(leftAstroids)
    

    for angle in orderedAstroids:
        dCount-=1
        if(dCount == 0):
            print(angle[0])
            break
        #print(angle)
        clear()
        l_astroidList.remove(angle[0])
        printWithList(l_astroidList,max_x,max_y)
        #sleep(1)

        

    

    


                 

        


        
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    args = parser.parse_args()

    main(args.file)




