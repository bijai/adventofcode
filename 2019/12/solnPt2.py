import re
import argparse
from copy import copy

regex = re.compile(r"<x=(-?\d+),\sy=(-?\d+),\sz=(-?\d+)>")

initMoon = {}
gxzero = None
gyzero = None 
gzzero = None

class Moon:
    id = 0
    def __init__(self, line):
        m = regex.match(line)
        self.x = int(m.group(1))
        self.y = int(m.group(2))
        self.z = int(m.group(3))

        self.vx = 0
        self.vy = 0
        self.vz = 0
        self.id = Moon.id
        Moon.id +=1
        self.pe =0
        self.ke =0
        self.computePE()
        self.computeKE()

    def computePE(self):
        self.pe= abs(self.x)+abs(self.y)+abs(self.z)

    def computeKE(self):
        self.ke= (abs(self.vx)+abs(self.vy)+abs(self.vz))

    
    def updatePos(self):
        self.x +=self.vx
        self.y +=self.vy
        self.z +=self.vz
        self.computePE()
        self.computeKE()

    def computeEnergy(self):
        return self.ke*self.pe

    def __repr__(self):
        return "{} : pos=<x={}, y={}, z={}>, vel=<x={}, y={}, z={}>".format(self.id,self.x,self.y,self.z,self.vx,self.vy,self.vz)

def printMoons(moons):
    print("\n".join([str(x) for x in moons]))

def computeVelocity(moons,axis,vaxis):
    for idx,moon in enumerate(moons):
        negEqual = 0
        nIdx = idx-1
        while(nIdx>=0 and getattr(moons[nIdx],axis) == getattr(moon,axis)):
            negEqual+=1
            nIdx-=1

        posEqual = 0
        pIdx = idx+1
        while(pIdx<len(moons) and getattr(moons[pIdx],axis) == getattr(moon,axis)):
            posEqual+=1
            pIdx+=1

        currentVel = getattr(moon,vaxis)
        updatedVel = currentVel + (-1*(idx-negEqual))+(len(moons)-(idx+posEqual)-1)
        setattr(moon,vaxis,updatedVel)


def computeVelocity2(moons,axis,vaxis):
    for moon1 in moons:
        less = 0
        more = 0
        for moon2 in moons:
            if(moon1==moon2 or getattr(moon1,axis) == getattr(moon2,axis)):
                continue
            if(getattr(moon1,axis)<getattr(moon2,axis)):
                more+=1
            else:
                less+=1
        currentVel = getattr(moon1,vaxis)
        updatedVel = currentVel + (-1*less)+more
        setattr(moon1,vaxis,updatedVel)

def applyStep(moons):
    
    moons.sort(key= lambda moon : moon.x)
    computeVelocity(moons,"x","vx")
    moons.sort(key= lambda moon : moon.y)
    computeVelocity(moons,"y","vy")
    moons.sort(key= lambda moon : moon.z)
    computeVelocity(moons,"z","vz")
    moons.sort(key= lambda moon : moon.id)

    for moon in moons:
        moon.updatePos()

    return moons

def applyStep2(moons):
    
    computeVelocity2(moons,"x","vx")
    computeVelocity2(moons,"y","vy")
    computeVelocity2(moons,"z","vz")

    for moon in moons:
        moon.updatePos()

    return moons

def readInput(file):
    moons = []
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        moons.append(Moon(line)) 
    return moons



    
def checkState(moons,step):
    global initMoon,gxzero,gyzero,gzzero
    zX =0
    zY =0
    zZ =0
    flag = True
    for moon in moons:
        if(moon.x==initMoon[moon.id].x and moon.vx==0):
            #print("X is all zero at {} for moon {}".format(step,moon.id))
            zX+=1
        if(moon.y==initMoon[moon.id].y and moon.vy==0):
            #print("Y is all zero at {} for moon {}".format(step,moon.id))
            zY+=1
        if(moon.z==initMoon[moon.id].z and moon.vz==0):
            #print("Z is all zero at {} for moon {}".format(step,moon.id))
            zZ+=1
        
        if(flag and (moon.pe!=0 or moon.ke!=0)):
            flag = False

    if(zX == len(moons)):
        if(gxzero==None):
            gxzero = step
        print("X is all zero at {}".format(step))
    if(zY == len(moons)):
        if(gyzero==None):
            gyzero = step
        print("Y is all zero at {}".format(step))
    if(zZ == len(moons)):
        if(gzzero==None):
            gzzero = step
        print("Z is all zero at {}".format(step))

    return flag
        

def saveInit(moons):
    global initMoon
    
    for moon in moons:
        initMoon[moon.id] = copy(moon)
        

def gcd(a,  b):
    if (a < 1 or b < 1):
        print("a or b is less than 1")
        return -1
    r = 0
    while True:
        r = a % b
        a = b
        b = r
        if(b == 0):
            break
    return a


def lcm(a,b):
    return (a*b)//gcd(a,b)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file',help='Input file to process')
    args = parser.parse_args()

    print(args.file)
    moons = readInput(args.file)

    saveInit(moons)

    steps = 0

    while True:
        steps+=1
        moons = applyStep(moons)
        if(True == checkState(moons,steps)):
            break
        if(steps%100000 == 0):
            print(steps)
        if(gxzero != None and gyzero!=None and gzzero!=None):
            print("Zeros at : x{} y{} z{} ".format(gxzero,gyzero,gzzero))
            print(lcm(lcm(gxzero,gyzero),gzzero))
            break








    