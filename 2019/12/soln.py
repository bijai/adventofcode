import re
import argparse

regex = re.compile(r"<x=(-?\d+),\sy=(-?\d+),\sz=(-?\d+)>")


class Moon:
    id = 0
    def __init__(self, line):
        m = regex.match(line)
        self.x = int(m.group(1))
        self.y = int(m.group(2))
        self.z = int(m.group(3))

        self.vx = 0
        self.vy = 0
        self.vz = 0
        self.id = Moon.id
        Moon.id +=1

    def updatePos(self):
        self.x +=self.vx
        self.y +=self.vy
        self.z +=self.vz

    def computeEnergy(self):
        return (abs(self.x)+abs(self.y)+abs(self.z))*(abs(self.vx)+abs(self.vy)+abs(self.vz))

    def __repr__(self):
        return "{} : pos=<x={}, y={}, z={}>, vel=<x={}, y={}, z={}>".format(self.id,self.x,self.y,self.z,self.vx,self.vy,self.vz)

def printMoons(moons):
    print("\n".join([str(x) for x in moons]))

def computeVelocity(moons,axis,vaxis):
    for idx,moon in enumerate(moons):
        negEqual = 0
        nIdx = idx-1
        while(nIdx>=0 and getattr(moons[nIdx],axis) == getattr(moon,axis)):
            negEqual+=1
            nIdx-=1

        posEqual = 0
        pIdx = idx+1
        while(pIdx<len(moons) and getattr(moons[pIdx],axis) == getattr(moon,axis)):
            posEqual+=1
            pIdx+=1

        currentVel = getattr(moon,vaxis)
        updatedVel = currentVel + (-1*(idx-negEqual))+(len(moons)-(idx+posEqual)-1)
        setattr(moon,vaxis,updatedVel)


def computeVelocity2(moons,axis,vaxis):
    for moon1 in moons:
        less = 0
        more = 0
        for moon2 in moons:
            if(moon1==moon2 or getattr(moon1,axis) == getattr(moon2,axis)):
                continue
            if(getattr(moon1,axis)<getattr(moon2,axis)):
                more+=1
            else:
                less+=1
        currentVel = getattr(moon1,vaxis)
        updatedVel = currentVel + (-1*less)+more
        setattr(moon1,vaxis,updatedVel)



def applyStep(moons):
    
    moons.sort(key= lambda moon : moon.x)
    computeVelocity(moons,"x","vx")
    moons.sort(key= lambda moon : moon.y)
    computeVelocity(moons,"y","vy")
    moons.sort(key= lambda moon : moon.z)
    computeVelocity(moons,"z","vz")
    moons.sort(key= lambda moon : moon.id)

    for moon in moons:
        moon.updatePos()

    return moons

def applyStep2(moons):
    
    computeVelocity2(moons,"x","vx")
    computeVelocity2(moons,"y","vy")
    computeVelocity2(moons,"z","vz")

    for moon in moons:
        moon.updatePos()

    return moons

def readInput(file):
    moons = []
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        moons.append(Moon(line))
    return moons
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file',help='Input file to process')
    args = parser.parse_args()

    print(args.file)
    moons = readInput(args.file)

    steps = 1000

    for step in range(steps):
        #print("After {} steps".format(step))
        #printMoons(moons)
        moons = applyStep(moons)

    
    print("After {} steps".format(steps))
    printMoons(moons)

    energy = 0
    for moon in moons:
        energy+=moon.computeEnergy()
    print(energy)






    