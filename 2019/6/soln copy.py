MAX = 99999999

def findPath(cur,dest,length):
    if(cur==dest):
        checked[cur] = length
        return length
    checked[cur] = MAX
    mini = MAX
    for child in space[cur].children:
        if(child not in checked):
            length = findPath(child,dest,length+1)
            if(length and length<mini):
                mini = length
                #print("{} > {} = {}".format(cur,dest,mini))

    if mini==MAX:
        if not space[cur].parent:
            return False
        if(space[cur].parent in checked):
            return False
    
    par_mini = findPath(space[cur].parent,dest,length+1)
    if(par_mini < mini):
        mini = par_mini


    if(mini):
        checked[cur] =mini
    
    return mini+1
    

class Body:
    def __init__(self,name):
        self.name = name
        self.parent = None
        self.children = set()
    
    def setParent(self,parent):
        self.parent = parent

    def __repr__(self):
        return self.name

    def __eq__(self, other):
        """Override the default Equals behavior"""
        return self.name == other.name

    def __ne__(self, other):
        """Override the default Unequal behavior"""
        return self.name != other.name

    def __hash__(self):
        return hash(self.__repr__())



space = {}
    
with open("tinput.txt") as f:
    f_lines = f.readlines()

for line in f_lines:
    temp = line.strip().split(')')
    pb = temp[0]
    sat = temp[1]
    if(sat not in space):
        space[sat] = Body(sat)
        space[sat].setParent(pb)
    
    if(pb in space):
        space[pb].children.add(sat)
    else:
        space[pb] = Body(pb)
        space[pb].children.add(sat)
    space[sat].parent = pb


count = 0
for body in space:
    i = space[body].name
    #test = i
    while(space[i].parent):
        count+=1
        i = space[i].parent
        #test+= "> "+i
    #print(test)

print(count)

source = space['YOU'].parent
dest = space['SAN'].parent

checked = {}

print (findPath(source,dest,0))
print(checked[source])
print(checked)



    

