
XMAX = 25
YMAX = 6
TOTAL = XMAX * YMAX

def init_layer(layerList):
    layerList.append({})
    layerId = len(layerList)-1
    for i in range(10):
        layerList[layerId][str(i)] =0
    return layerId

with open("input.txt") as f:
    imageline = f.readlines()

imageline = imageline[0].strip()


layer_list = []

digits = len(imageline)
j=0
layerid = 0
for i in range(digits):
    if(i%(TOTAL) == 0): # new layer
        j=0
        layerid = init_layer(layer_list)
    layer_list[layerid][imageline[i]]+=1
    

min0 = None

for idx,layer in enumerate(layer_list):
    zero_count = layer['0']
    if(not min0 or zero_count<min0):
        min0Layer = idx
        min0 = zero_count

OneS = layer_list[min0Layer]['1']
TwoS = layer_list[min0Layer]['2']

print("Least Zero Count {}, Layer {}, 1*2 {}".format(min0,min0Layer,OneS*TwoS))









