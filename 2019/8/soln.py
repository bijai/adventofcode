from PIL import Image

XMAX = 25
YMAX = 6
TOTAL = XMAX * YMAX

BLACK = '0'
WHITE = '1'
TRANSPARENT = '2'

def initImage(image,value):
    for i in range(TOTAL):
        image.append(value)

def init_layer(layerList):
    layerList.append({})
    layerId = len(layerList)-1
    for i in range(10):
        layerList[layerId][str(i)] =0
    return layerId

with open("input.txt") as f:
    imageline = f.readlines()

imageline = imageline[0].strip()


layer_list = []

whole = len(imageline)
j=0
layerid = 0

result = []
initImage(result,TRANSPARENT)


for i in range(whole):
    j = i%(TOTAL)
    if result[j] == BLACK or result[j] == WHITE:
        continue 
    if(imageline[i] == BLACK or imageline[i] == WHITE):
        result[j] = imageline[i]
    
img = Image.new('RGBA', (XMAX, YMAX), color = (128,128,128,0))  

for i in range(len(result)):
    x = i%(XMAX)
    y = int(i/XMAX)
    #print(x,y)
    if(result[i] == BLACK):
        img.putpixel((x,y),(0,0,0,255))
    elif(result[i] == WHITE):
        img.putpixel((x,y),(255,255,255,255))
    
img.save("output.png")    

for j in range(YMAX):
    print(' '.join(result[XMAX*j:XMAX*j+XMAX]))









