import java.io.File
import java.io.InputStream

fun main(args: Array<String>) {
    val inputStream: InputStream = File(args.first()).inputStream()

    var sum = 0
    inputStream.bufferedReader().forEachLine {
        val ballStr = it.split(":")[1]
        val balls =
                ballStr.split(";")
                        .map { it.split(",").map { it.trim().split(" ") } }
                        .flatten()
                        .groupBy({ it[1] }, { Integer.parseInt(it[0]) })
        sum += balls.values.map { it.max() }.reduce { mul,elem -> mul*elem }
    }
    println(sum)
}
