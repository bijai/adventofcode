import java.io.File
import java.io.InputStream

fun main(args: Array<String>) {
    val inputStream: InputStream = File(args.first()).inputStream()

    var sum = 0
    inputStream.bufferedReader().forEachLine {
        val (gameIdStr, ballStr) = it.split(":")
        val gameId = gameIdStr.filter { it.isDigit() }
        val balls =
                ballStr.split(";")
                        .map { it.split(",").map { it.trim().split(" ") } }
                        .flatten()
                        .groupBy({ it[1] }, { Integer.parseInt(it[0]) })

        // print("\n$gameId : $balls : ")

        if (balls.get("blue")?.any { it > 14 } ?: false) {
            return@forEachLine
        }
        if (balls.get("green")?.any { it > 13 } ?: false) {
            return@forEachLine
        }
        if (balls.get("red")?.any { it > 12 } ?: false) {
            return@forEachLine
        }

        // print("valid")

        sum += Integer.parseInt(gameId)
    }
    println(sum)
}
