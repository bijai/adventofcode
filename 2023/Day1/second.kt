import java.io.File
import java.io.InputStream

fun main(args: Array<String>) {
  val inputStream: InputStream = File(args.first()).inputStream()

  var sum = 0
  inputStream.bufferedReader().forEachLine {
    var beg = ' '
    var end = ' '

    //print("$it : ")

    run startLoop@{
      it.forEachIndexed { i, c ->
        if (c >= '0' && c <= '9') {
          beg = c
          return@startLoop
        }
        when (c) {
          'o' ->
              if (i + 3 < it.length && it.subSequence(i, i + 3) == "one") {
                beg = '1'
                return@startLoop
              }
          't' ->
              if (i + 3 < it.length && it.subSequence(i, i + 3) == "two") {
                beg = '2'
                return@startLoop
              } else if (i + 5 < it.length && it.subSequence(i, i + 5) == "three") {
                beg = '3'
                return@startLoop
              }
          'f' ->
              if (i + 4 < it.length && it.subSequence(i, i + 4) == "four") {
                beg = '4'
                return@startLoop
              } else if (i + 4 < it.length && it.subSequence(i, i + 4) == "five") {
                beg = '5'
                return@startLoop
              }
          's' ->
              if (i + 3 < it.length && it.subSequence(i, i + 3) == "six") {
                beg = '6'
                return@startLoop
              } else if (i + 5 < it.length && it.subSequence(i, i + 5) == "seven") {
                beg = '7'
                return@startLoop
              }
          'e' ->
              if (i + 5 < it.length && it.subSequence(i, i + 5) == "eight") {
                beg = '8'
                return@startLoop
              }
          'n' ->
              if (i + 4 < it.length && it.subSequence(i, i + 4) == "nine") {
                beg = '9'
                return@startLoop
              }
        }
      }
    }
    //print("$beg : ")
    //print("${it.reversed()} : ")

    run endLoop@ {

      it.reversed().forEachIndexed { ir, c ->
        val i = it.length - ir - 1
        if (c >= '0' && c <= '9') {
          end = c
          return@endLoop
        }
        when (c) {
          'o' ->
              if (i + 3 <= it.length && it.subSequence(i, i + 3) == "one") {
                end = '1'
                return@endLoop
              }
          't' ->
              if (i + 3 <= it.length && it.subSequence(i, i + 3) == "two") {
                end = '2'
                return@endLoop
              } else if (i + 5 <= it.length && it.subSequence(i, i + 5) == "three") {
                end = '3'
                return@endLoop
              }
          'f' ->
              if (i + 4 <= it.length && it.subSequence(i, i + 4) == "four") {
                end = '4'
                return@endLoop
              } else if (i + 4 <= it.length && it.subSequence(i, i + 4) == "five") {
                end = '5'
                return@endLoop
              }
          's' ->
              if (i + 3 <= it.length && it.subSequence(i, i + 3) == "six") {
                end = '6'
                return@endLoop
              } else if (i + 5 <= it.length && it.subSequence(i, i + 5) == "seven") {
                end = '7'
                return@endLoop
              }
          'e' ->
              if (i + 5 <= it.length && it.subSequence(i, i + 5) == "eight") {
                end = '8'
                return@endLoop
              }
          'n' ->
              if (i + 4 <= it.length && it.subSequence(i, i + 4) == "nine") {
                end = '9'
                return@endLoop
              }
        }
      }
    }

    
    val ans = "${beg}${end}"
    //println("${end} : ${ans}")

    sum += Integer.parseInt(ans)
  }
  println(sum)
}
