import java.io.File
import java.io.InputStream

fun main(args: Array<String>) {
    val inputStream: InputStream = File(args.first()).inputStream()

    var sum = 0
    inputStream.bufferedReader().forEachLine { 
      val nums = it.filter { it >= '0' && it<='9' }
      val ans = "${nums.first()}${nums.last()}"
      //println(Integer.parseInt(ans))
      sum+=Integer.parseInt(ans)
    } 
    println(sum)
}