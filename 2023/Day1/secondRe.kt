import java.io.File
import java.io.InputStream

fun main(args: Array<String>) {
  val digitsMap = mapOf(
        "zero"  to "0", "0" to "0",
        "one"   to "1", "1" to "1",
        "two"   to "2", "2" to "2",
        "three" to "3", "3" to "3",
        "four"  to "4", "4" to "4",
        "five"  to "5", "5" to "5",
        "six"   to "6", "6" to "6",
        "seven" to "7", "7" to "7",
        "eight" to "8", "8" to "8",
        "nine"  to "9", "9" to "9"
    )

  val inputStream: InputStream = File(args.first()).inputStream()
  var sum = 0
  inputStream.bufferedReader().forEachLine {
    val beg = digitsMap.get(it.findAnyOf(digitsMap.keys)?.second)
    val end = digitsMap.get(it.findLastAnyOf(digitsMap.keys)?.second)
    sum += Integer.parseInt("$beg$end")
  }
  println(sum)
}