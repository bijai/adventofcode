import math
MOVE_NORTH = 'N'
MOVE_SOUTH = 'S'
MOVE_WEST = 'W'
MOVE_EAST = 'E'
TURN_LEFT = 'L'
TURN_RIGHT = 'R'
MOVE_FORWARD = 'F'

X = 0
Y = 1

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]

def computeCoord(curCoord,angle,disp):
    x = 0
    y = 0
    if 0 <= angle <= 90 :
        x+= math.sin(math.radians(angle)) * disp
        y+= math.cos(math.radians(angle)) * disp
    elif 90 <= angle <= 180 :
        angle -= 90
        x+= math.cos(math.radians(angle)) * disp
        y-= math.sin(math.radians(angle)) * disp
    elif 180 <= angle <= 270 :
        angle-=180
        x-= math.sin(math.radians(angle)) * disp
        y-= math.cos(math.radians(angle)) * disp
    elif 270 <= angle <= 360 :
        angle-=270
        x-= math.cos(math.radians(angle)) * disp
        y+= math.sin(math.radians(angle)) * disp
    x= int(x)
    y = int(y)
    return (curCoord[0]+x,curCoord[1]+y)

def main():
    actionList = readFile("input.txt")
    curCoordinate = (0,0)
    curHeading = 90

    for action in actionList:
        command = action[:1]
        disp = int(action[1:])
        prevCoord = curCoordinate
        prevHeading = curHeading
        if(command == MOVE_NORTH):
            curCoordinate = (curCoordinate[X],curCoordinate[Y]+disp)
        elif command == MOVE_SOUTH:
            curCoordinate = (curCoordinate[X],curCoordinate[Y]-disp)
        elif command == MOVE_WEST:
            curCoordinate = (curCoordinate[X]-disp,curCoordinate[Y])
        elif command == MOVE_EAST:
            curCoordinate = (curCoordinate[X]+disp,curCoordinate[Y])
        elif command == TURN_LEFT:
            curHeading -= disp
            curHeading %=360 
        elif command == TURN_RIGHT:
            curHeading += disp
            curHeading %=360
        elif command == MOVE_FORWARD:
            curCoordinate = computeCoord(curCoordinate,curHeading,disp)

        print(f'{prevCoord} {prevHeading} : {command} {disp} : {curCoordinate} {curHeading}')

    print(abs(curCoordinate[0]) + abs(curCoordinate[1]))
        


if __name__ == "__main__":
    main()