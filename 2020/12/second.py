import math
MOVE_NORTH = 'N'
MOVE_SOUTH = 'S'
MOVE_WEST = 'W'
MOVE_EAST = 'E'
TURN_LEFT = 'L'
TURN_RIGHT = 'R'
MOVE_FORWARD = 'F'

X = 0
Y = 1

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]

def computeNewWayPoint(curCoord,disp):

    if abs(disp) == 180 :
        return (curCoord[X]*-1,curCoord[Y]*-1)
    
    r = abs(curCoord[X])**2 + abs(curCoord[Y])**2
    r = math.sqrt(r)

    t1 = math.asin(abs(curCoord[Y])/r)
    t2 = t1 + math.radians(disp)

    x = round(r * math.cos(t2))
    y = round(r * math.sin(t2))

    return(x,y)

    

def main():
    actionList = readFile("input.txt")
    curCoordinate = (0,0)
    wayPointRelCoord = (10,1)

    for action in actionList:
        command = action[:1]
        disp = int(action[1:])
        prevCoord = curCoordinate
        prevWay = wayPointRelCoord
        if(command == MOVE_NORTH):
            wayPointRelCoord = (wayPointRelCoord[X],wayPointRelCoord[Y]+disp)
        elif command == MOVE_SOUTH:
            wayPointRelCoord = (wayPointRelCoord[X],wayPointRelCoord[Y]-disp)
        elif command == MOVE_WEST:
            wayPointRelCoord = (wayPointRelCoord[X]-disp,wayPointRelCoord[Y])
        elif command == MOVE_EAST:
            wayPointRelCoord = (wayPointRelCoord[X]+disp,wayPointRelCoord[Y])
        elif command == TURN_LEFT:
            wayPointRelCoord = computeNewWayPoint(wayPointRelCoord,-1 * disp)
        elif command == TURN_RIGHT:
            wayPointRelCoord = computeNewWayPoint(wayPointRelCoord, disp)
        elif command == MOVE_FORWARD:
            curCoordinate = (curCoordinate[X] + wayPointRelCoord[X]*disp,curCoordinate[Y] + wayPointRelCoord[Y]*disp)

        print(f'{prevCoord} {prevWay} : {command} {disp} : {curCoordinate} {wayPointRelCoord}')

    print(abs(curCoordinate[X]) + abs(curCoordinate[Y]))
        


if __name__ == "__main__":
    main()
    #computeNewWayPoint((-24, 41),-270)

# 98651 too low
# 75299 too low
# 24477
# 187137 too high