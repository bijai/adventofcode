with open('input.txt') as f:
    lines = f.readlines()

count = 0
for line in lines:
    #print(line)
    arr = line.split(':')
    password = arr[1].strip()
    constraints = arr[0].split()#1-3 l
    lRange = constraints[0].split('-')
    #print(lRange)
    pos1 = int(lRange[0])
    pos2 = int(lRange[1])
    letter = constraints[1]
    if password[pos1-1] == letter and password[pos2-1] != letter:
        count+=1
    elif password[pos1-1] != letter and password[pos2-1] == letter:
        count+=1
print(count)
