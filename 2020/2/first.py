with open('input.txt') as f:
    lines = f.readlines()

count = 0
for line in lines:
    #print(line)
    arr = line.split(':')
    password = arr[1].strip()
    constraints = arr[0].split()#1-3 l
    lRange = constraints[0].split('-')
    #print(lRange)
    lMin = int(lRange[0])
    lMax = int(lRange[1])
    letter = constraints[1]
    lCount = 0
    for l in password:
        if l==letter:
            lCount+=1
    if lMin <= lCount <= lMax :
        count+=1
print(count)
