

class Group:
    def __init__(self,groupStrArr):
        self.personSets = []
        self.groupSet = set()
        self.parseGroupStr(groupStrArr)
        self.uniqueQ = set()
        self.computeUnique()


    def parseGroupStr(self,groupStrArr):
        for person in groupStrArr:
            personSet = set()
            for ques in person:
                personSet.add(ques)
            self.personSets.append(personSet)
        self.groupSet = set.union(*self.personSets) 


    def computeUnique(self):
        if len(self.personSets) <= 1:
            self.uniqueQ = self.groupSet
        else:
            self.uniqueQ = self.personSets[0]
            for person in self.personSets[1:]:
                self.uniqueQ.intersection_update(person)
    

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]


def main():
    groupLines = readFile('input.txt')
    groupArray = []
    processed = []
    for line in groupLines:
        if not line:
            if len(groupArray) > 0:
                processed.append(Group(groupArray))
                groupArray=[]
        else:
            groupArray.append(line)
    if len(groupArray) > 0:
        processed.append(Group(groupArray))

    count = 0
    uniqueCount = 0
    for group in processed:
        #print(group.groupSet)
        count += len(group.groupSet)
        uniqueCount += len(group.uniqueQ)

    print(f'Total count : {count}')
    print(f'Total unique Q : {uniqueCount}')

if __name__ == "__main__":
    main()