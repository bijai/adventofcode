
def main():
    with open('input.txt') as f:
        lines = f.readlines()

    lines = [int(x) for x in lines]

    for i in range(len(lines)):
        for j in range(i+1,len(lines)):
            print(f'[{i}]: {lines[i]} + [{j}]: {lines[j]}')
            if(lines[i]+lines[j] == 2020):
                print(f"Found {lines[i]} {lines[j]} ={lines[i]*lines[j]}")
                return lines[i]*lines[j]


if __name__ == "__main__":
    main()
