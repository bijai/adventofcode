

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]

class XMAS:
    def __init__(self,preambleLength,message):
        self.pLength = preambleLength
        #self.message = [int(x) for x in message]
        self.message = message
        self.sumCache = {}
        self.initCache()
        self.windowStart = 0


    def initCache(self):
        for i in range(self.pLength-1):
            for j in range(i+1,self.pLength):
                self.sumCache[self.message[i]+self.message[j]] = (i,j)

    def rollOver(self,count=1):
        self.windowStart +=1
        if(self.windowStart+self.pLength > len(self.message)):
            return False
        self.dumpCache('Before trim')
        self.sumCache = dict(list(self.sumCache.items())[self.pLength-1:])
        self.dumpCache('After trim')
        for i in range(self.windowStart,self.pLength+self.windowStart-1):
            j = self.pLength+self.windowStart-1
            self.sumCache[self.message[i]+self.message[j]] = (i,j)
        self.dumpCache('After Add')
        return True
    
    def check(self,index=None):
        if index is None:
            index = self.windowStart+self.pLength
        if(self.message[index] not in self.sumCache):
            print(f'{self.message[index]}@[{index}] not computable with preamble')
            return False
        return True

    def dump(self):
        return f'pLength[{self.pLength}] windowStart[{self.windowStart}] [{self.message[self.windowStart]},{self.message[self.windowStart+self.pLength]}]'

    def dumpCache(self,msg):
        ketList = list(self.sumCache.items())
        print('-'*5,msg)
        for i in ketList:
            print(f'[{i}]')
        print('-'*5)



        

def main():
    numbers = readFile('test.txt')
    comp = XMAS(5,numbers)

    while comp.check() and comp.rollOver():
        print(comp.dump())
        




if __name__ == "__main__":
    main()