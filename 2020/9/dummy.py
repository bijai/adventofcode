

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]


def main():
    numbers = readFile('dummy.txt')
    print(len(numbers))
    for i in range(len(numbers)):
        for j in range(i+1,len(numbers)-1):
            print(f'{numbers[i]} {numbers[j]} {numbers[i]+numbers[j]}')
        

#9948 too low


if __name__ == "__main__":
    main()