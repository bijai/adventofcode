

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]

def findSeries(pNumbers,total):
    for i in range(len(pNumbers)-1):
        lSum = pNumbers[i]
        smallest = pNumbers[i]
        largest = pNumbers[i]
        for j in range(i+1,len(pNumbers)):
            lSum += pNumbers[j]
            if(pNumbers[j] > largest):
                largest = pNumbers[j]
            if(pNumbers[j] < smallest):
                smallest = pNumbers[j]

            if lSum == total:
                print(pNumbers[i:j+1])
                print(f'Found {pNumbers[i]} {pNumbers[j]} = {smallest+largest}')
                return (i,j)
            elif lSum > total:
                break
            



def main():
    numbers = readFile('input.txt')
    #10884537
    findSeries(numbers,10884537)
#1196645 too low

if __name__ == "__main__":
    main()