

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]

class XMAS:
    def __init__(self,preambleLength,message):
        self.pLength = preambleLength
        self.message = message
        self.windowStart = 0
        self.sumSet = set()


    def rollOver(self,count=1):
        self.windowStart +=1
        if(self.windowStart+self.pLength > len(self.message)):
            return False
        return True

    def buildSum(self):
        self.sumSet = set()
        for i in range(self.windowStart,self.windowStart+self.pLength-1):
            for j in range(i+1,self.windowStart+self.pLength):
                if j == i or self.message[j] == self.message[i]:
                    continue
                self.sumSet.add(self.message[i] + self.message[j])

    
    def check(self,index=None):
        if index is None:
            index = self.windowStart+self.pLength
        self.buildSum()
        if(self.message[index] not in self.sumSet):
            print(f'{self.message[index]}@[{index}] not computable with preamble')
            return False
        return True

    def dump(self):
        return f'pLength[{self.pLength}] windowStart[{self.windowStart}] [{self.message[self.windowStart]},{self.message[self.windowStart+self.pLength-1]}]'
        

def main():
    numbers = readFile('input.txt')
    comp = XMAS(25,numbers)

    while comp.check() and comp.rollOver():
        print(comp.dump())
        

#9948 too low


if __name__ == "__main__":
    main()