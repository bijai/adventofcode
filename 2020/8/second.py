
SUCCESS = 0
TERMINATED = 1
INFI_LOOP = 2


class Computer:
    def __init__(self,prog):
        self.reset(prog)

    def reset(self,prog=None):
        self.IP = 0
        self.prevIP = 0
        self.acc = 0
        self.opCode = None
        self.param = None
        self.executedOffset = set()
        if prog is not None:
            self.prog = prog
            self.lastInstruction = len(self.prog)

    def nextInstruction(self):
        self.prevIP = self.IP
        if self.IP == self.lastInstruction:
            return TERMINATED
        iStr = self.prog[self.IP]
        if self.IP in self.executedOffset:
            print(f'Infinite loop detected IP[{self.IP}] ACC[{self.acc}]')
            return INFI_LOOP
        self.executedOffset.add(self.IP)
        self.opCode = iStr.split()[0]
        self.param = iStr.split()[1]

        if self.opCode == 'nop':
            self.IP+=1
        elif self.opCode == 'acc':
            lParam = int(self.param.strip())
            self.acc+=lParam
            self.IP+=1
        elif self.opCode == 'jmp':
            lParam = int(self.param.strip())
            self.IP+=lParam
        else:
            raise Exception(f'Unknown opcode {iStr}')
        #print(f'{iStr}\t\t{self.IP}-{self.acc}')
        return SUCCESS

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]


def main():

    orgProg = readFile('input.txt')
    
    comp = Computer(None)
    lastLoc = []

    comp.reset(orgProg)
    while comp.nextInstruction() == SUCCESS:
        if(comp.opCode == 'jmp' or comp.opCode == 'nop'):
            lastLoc.append(comp.prevIP)

    while len(lastLoc)> 0:
        change = lastLoc.pop()
        modprog = orgProg.copy()
        opCode = modprog[change].split()[0].strip()
        if(opCode == 'jmp'):
            modprog[change] = f'nop {modprog[change][4:]}'
        elif opCode == 'nop':
            modprog[change] = f'jmp {modprog[change][4:]}'
        comp.reset(modprog)
        while (status := comp.nextInstruction()) == SUCCESS:
            pass
        if status == TERMINATED:
            print(f'loc to modify {change} : acc {comp.acc}')
            break
        
           
        

if __name__ == "__main__":
    main()