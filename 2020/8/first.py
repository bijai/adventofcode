

class Computer:
    def __init__(self,prog):
        self.IP = 0
        self.acc = 0
        self.prog = prog
        self.lastInstruction = len(self.prog)
        self.executedOffset = set()

    def nextInstruction(self):
        if self.IP == self.lastInstruction:
            return False
        iStr = self.prog[self.IP]
        if self.IP in self.executedOffset:
            raise Exception(f'Infinite loop detected IP[{self.IP}] ACC[{self.acc}]')
        self.executedOffset.add(self.IP)
        opCode = iStr.split()[0]
        param = iStr.split()[1]

        if opCode == 'nop':
            self.IP+=1
        elif opCode == 'acc':
            param = int(param.strip())
            self.acc+=param
            self.IP+=1
        elif opCode == 'jmp':
            param = int(param.strip())
            self.IP+=param
        else:
            raise Exception(f'Unknown opcode {iStr}')
        print(f'{iStr}\t\t{self.IP}-{self.acc}')
        return True

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]


def main():
    comp = Computer(readFile('test.txt'))

    while comp.nextInstruction():
        pass
    print('Program terminated')
        

if __name__ == "__main__":
    main()