
gMap = []
gMaxX = 0

OPEN = '.'
TREE = '#'

def isTree(x,y):
    if(y > len(gMap)):
        raise Exception(f"Out of bounds ({x},{y}) for max y {len(gMap)}")
    return gMap[y][x%gMaxX]==TREE


def readInput(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]
    
def traverse(slopeX,slopeY):
    x = 0
    y = 0

    count = 0
    while(y < len(gMap)):
        if(isTree(x,y)):
            count +=1
        x = x+slopeX
        y = y+slopeY

    return count
    

'''
Right 1, down 1.
Right 3, down 1. (This is the slope you already checked.)
Right 5, down 1.
Right 7, down 1.
Right 1, down 2.
'''

def main():
    global gMap,gMaxX
    gMap = readInput('input.txt')
    gMaxX = len(gMap[0])
    lSlopes = [(1,1),(3,1),(5,1),(7,1),(1,2)]
    product = 1
    for slope in lSlopes:
        treeCount = traverse(slope[0],slope[1])
        print(treeCount)
        product *=treeCount
    print(product)

    

    
    



if __name__ == "__main__":
    main()