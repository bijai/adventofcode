import copy

EMPTY_SEAT = 'L'
OCC_SEAT = '#'
FLOOR = '.'

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [list(x.strip()) for x in lines]

def lookAround(pMap,x,y):
    freeSeatCount = 0 
    occSeatCount = 0
    floorCount = 0
    for i in range(-1,2):
        if x+i < 0 or x+i >= len(pMap):
            continue
        for j in range(-1,2):
            if y+j < 0 or y+j >= len(pMap[x+i]):
                continue
            if(i==0 and j==0):
                continue
            if pMap[x+i][y+j] == EMPTY_SEAT:
                freeSeatCount+=1
            elif pMap[x+i][y+j] == OCC_SEAT:
                occSeatCount+=1
            elif pMap[x+i][y+j] == FLOOR:
                floorCount+=1
            else:
                raise Exception(f'What is [{pMap[x+i][y+j]}]')
    return (freeSeatCount,occSeatCount,floorCount)


def compareMap(pMap1,pMap2):
    for i in range(len(pMap1)):
        for j in range(len(pMap1[i])):
            if( pMap1[i][j] != pMap2[i][j] ):
                return False
    return True


def printMap(pMap):
    for line in pMap:
        print("".join(line))
    print("\n")

def fillMap(pMap):
    orgMap = copy.deepcopy(pMap)
    for i in range(len(orgMap)):
        for j in range(len(orgMap[i])):
            free,occ,floor = lookAround(orgMap,i,j)
            if(orgMap[i][j] == EMPTY_SEAT and occ == 0):
                pMap[i][j] = OCC_SEAT
            
def emptyMap(pMap):
    orgMap = copy.deepcopy(pMap)
    for i in range(len(orgMap)):
        for j in range(len(orgMap[i])):
            free,occ,floor = lookAround(orgMap,i,j)
            if(orgMap[i][j] == OCC_SEAT and occ >= 4):
                pMap[i][j] = EMPTY_SEAT



def compute(pMap):
    fillMap(pMap)
    #printMap(pMap)
    emptyMap(pMap)
    #printMap(pMap)
    #input("Press Enter")



def main():
    lMap = readFile('input.txt')

    iterCount = 0
    while True:
        cMap = copy.deepcopy(lMap)
        compute(cMap)
        if(compareMap(lMap,cMap)):
            break
        lMap = cMap
        iterCount+=1
        print(iterCount)
    
    occCount = 0
    for i in range(len(lMap)):
        for j in range(len(lMap[i])):
            if(lMap[i][j] == OCC_SEAT):
                occCount+=1
    print(occCount)
        






if __name__ == "__main__":
    main()


