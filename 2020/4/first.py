'''
byr (Birth Year)
iyr (Issue Year)
eyr (Expiration Year)
hgt (Height)
hcl (Hair Color)
ecl (Eye Color)
pid (Passport ID)
cid (Country ID)'''

import re

kvPatt = re.compile(r'(\w{3}):(\S+)')

EYECOLOR = set(['amb','blu','brn','gry','grn','hzl','oth'])

def rangeCheck(val,pmin,pmax):
    return pmin <= val <= pmax

class passport:
    def __init__(self,ppt_str):
        self.propMap = {}
        self.parse(ppt_str)

    def parse(self,ppt_str):
        matches = kvPatt.findall(ppt_str)
        for match in matches:
            self.propMap[match[0]] = match[1]

    def valid(self):
        try:
            if 'byr' not in self.propMap:
                return False
            byr = self.propMap['byr']
            if len(byr) != 4:
                return False
            if not rangeCheck(int(byr),1920,2002):
                return False

            if 'iyr' not in self.propMap:
                return False
            
            iyr = self.propMap['iyr']
            if len(iyr) != 4:
                return False
            if not rangeCheck(int(iyr),2010,2020):
                return False

            if 'eyr' not in self.propMap:
                return False
            eyr = self.propMap['eyr']
            if len(eyr) != 4:
                return False
            if not rangeCheck(int(eyr),2020,2030):
                return False

            if 'hgt' not in self.propMap:
                return False
            hgt = self.propMap['hgt']
            val = int(hgt[:-2])
            unit = hgt[-2:]
            if unit=='in': 
                if not rangeCheck(val,59,76):
                    return False
            elif unit=='cm': 
                if not rangeCheck(val,150,193):
                    return False
            else:
                return False

            
            if 'hcl' not in self.propMap:
                return False
            hcl = self.propMap['hcl']
            if hcl[:1] != '#':
                return False
            val = int(hcl[1:],16)
            

            if 'ecl' not in self.propMap:
                return False
            ecl = self.propMap['ecl']
            if ecl not in EYECOLOR:
                return False
            
            if 'pid' not in self.propMap:
                return False
            pid = self.propMap['pid']
            if len(pid) != 9:
                return False
            val = int(pid)
            return True
        except Exception as e:
            print(e)
            return False



def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    
    return [x.strip() for x in lines]


def main():
    sLines = readFile('input.txt')
    pptArray = []
    tempStr = ""
    validCount = 0
    for line in sLines:
        if not line:
            #mark prev as ppt
            if tempStr:
                pptArray.append(passport(tempStr))
                if(pptArray[-1].valid()):
                    validCount+=1
                tempStr = ''
        else:
            tempStr += f' {line}'

    if tempStr:
        pptArray.append(passport(tempStr))
        if(pptArray[-1].valid()):
            validCount+=1
        
    print(len(pptArray))
    print(validCount)


        
        

if __name__ == "__main__":
    main()