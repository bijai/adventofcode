import re
targetBagPatt = re.compile(r'(\d) (\w+) (\w+) bags?')

gData = {}

class Bag:
    def __init__(self,pattern,color):
        self.pattern = pattern
        self.color = color
        self.contains = []
        self.inside = set()

    def addBag(self,pBag,count):
        self.contains.append((pBag,count))

    def addInside(self,sourceBag):
        self.inside.add(sourceBag)

    def dumpStr(self):
        containsStr = ','.join([f'{x[0]}:{x[1]}' for x in self.contains])
        return f'[{self.pattern}{self.color}] = [{containsStr}]'


def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [x.strip() for x in lines]

def getGBag(pattern,color):
    if f'{pattern} {color}' not in gData:
        gData[f'{pattern} {color}'] = Bag(pattern,color)
    return gData[f'{pattern} {color}']



def ruleToBag(pRule):
    splitArr = pRule.split('contain')
    if(len(splitArr) != 2):
        raise Exception(pRule)
    sourceBagArr = splitArr[0].strip().split()
    pattern = sourceBagArr[0]
    color = sourceBagArr[1]

    lBagObj = getGBag(pattern,color)

    if( splitArr[1] != ' no other bags.'):
        matches = targetBagPatt.findall(splitArr[1])
        for match in matches:
            #print(match[0],match[1],match[2])
            getGBag(match[1],match[2]).addInside(f'{pattern} {color}')
            lBagObj.addBag(f'{match[1]} {match[2]}',match[0])


def findSources(srcStr):
    toSearch = set(gData[srcStr].inside)
    searched = set()
    while len(toSearch) >0:
        bag = toSearch.pop()
        bagSearch = gData[bag].inside.copy()
        bagSearch.difference_update(toSearch)
        bagSearch.difference_update(searched)
        toSearch = toSearch.union(bagSearch)
        searched.add(bag)
    return searched

def main():
    lRules = readFile('input.txt')
    for rule in lRules:
        ruleToBag(rule)
    
    tempArr = findSources('shiny gold')
    print(tempArr)
    print(len(tempArr))

    

if __name__ == "__main__":
    main()