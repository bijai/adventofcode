
gCombArr = []
gCombCache = {}

def recurseArr(pArr,index):
    if(index == len(pArr)):
        gCombArr.append(pArr.copy())
    else:
        if index<2 or (pArr[index-1] != 0 or pArr[index-2] != 0):
            recurseArr([x if ind!=index else 0 for ind,x in enumerate(pArr)],index+1)
        pArr[index]=1
        recurseArr([x if ind!=index else 1 for ind,x in enumerate(pArr)],index+1)

def genComb(length):
    global gCombArr

    if (length in gCombCache):
        return gCombCache[length]
    if(length<1):
        return 1

    gCombArr = []
    arr = [0]*length
    print(f'Combinations for [{length}]')
    recurseArr(arr,0)
    for comb in gCombArr:
        print(comb)
    print('-----')
    #print(f'{len(gCombArr)} : {2**length} : {2**length - len(gCombArr)}')
    gCombCache[length] = len(gCombArr)
    return(len(gCombArr))
    


def main():
    for i in range(10):
        genComb(i)
        

if __name__ == "__main__":
    main()


    # 2 > 3
    # 
    