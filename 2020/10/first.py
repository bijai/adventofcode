

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]


def customFact(number):
    lSum = 1
    while number > 2:
        lSum*=number
    


def main():
    adapters = readFile('test.txt')
    adapters.sort()
    if(adapters[0] != 0):
        adapters.insert(0,0)
    adapters.append(adapters[-1]+3)
    diffList = []
    countMap = {0:0,1:0,2:0,3:0}
    totalConsCount =0
    consCount = 0
    for index,adapter in enumerate(adapters[1:]):
        lDiff = adapter - adapters[index]
        countMap[lDiff]+=1
        diffList.append(lDiff)
        if(lDiff == 1):
            consCount+=1
        else:
            if(consCount > 1):
                totalConsCount += consCount*consCount-1
            consCount = 0

    print(countMap)
    print(countMap[1]*countMap[3])
    print(diffList)
    print(totalConsCount)
    print(2**totalConsCount)

if __name__ == "__main__":
    main()


    # 2 > 3
    # 
    