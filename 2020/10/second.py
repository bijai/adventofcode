from comb import genComb

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    return [int(x.strip()) for x in lines]


def customFact(number):
    lSum = 1
    while number > 2:
        lSum*=number
    


def main():
    adapters = readFile('input.txt')
    adapters.sort()
    print(adapters)
    if(adapters[0] != 0):
        adapters.insert(0,0)
    adapters.append(adapters[-1]+3)
    diffList = []
    totalConsCount =1
    consCount = 0
    for index,adapter in enumerate(adapters[1:]):
        lDiff = adapter - adapters[index]
        diffList.append(lDiff)
        if(lDiff == 1):
            consCount+=1
        else:
            if(consCount > 1):
                totalConsCount *= genComb(consCount-1)
                print(f'Index {index} - {consCount-1} - {totalConsCount}')
            consCount = 0

    print(diffList)
    print(totalConsCount)

if __name__ == "__main__":
    main()


    # 2 > 3
    # 
    