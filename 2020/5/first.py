#!python3.9
import math

ROW_FRONT = 'F'
ROW_BACK = 'B'

ROW_LEFT = 'L'
ROW_RIGHT = 'R'

def readFile(filename):
    with open(filename) as f:
        lines = f.readlines()
    
    return [x.strip() for x in lines]

def binaryRecurse(pStr,pMin,pMax,pLow,pHigh):
    if (len(pStr)==0 or pMin>=pMax):
        #print(f"End of bin search : {pMin}, {pMax}")
        return pMax
    
    half = math.ceil((pMax - pMin)/2)
    if pStr[0] == pLow:
        return binaryRecurse(pStr[1:],pMin,pMax-half,pLow,pHigh)
    elif pStr[0] == pHigh:
        return binaryRecurse(pStr[1:],pMin+half,pMax,pLow,pHigh)
    else:
        raise Exception(f'Invalid character on binary search {pStr} ({pLow}{pHigh})')
    

def computeSeatId(seatCode):
    row = binaryRecurse(seatCode[:7],0,127,ROW_FRONT,ROW_BACK)
    col = binaryRecurse(seatCode[-3:],0,7,ROW_LEFT,ROW_RIGHT)
    #print(f'{row} {col} = {row*8 + col}')
    return row*8 + col

def main():
    seatCodes = readFile('input.txt')
    seatList = []
    maxSeatId = 0
    for seatCode in seatCodes:
        seatId = computeSeatId(seatCode)
        #print(seatId)
        if(seatId>maxSeatId):
            maxSeatId = seatId
        seatList.append(seatId)
        
    print(f'Max seat ID {maxSeatId}')

    seatList.sort()

    prevSeat = seatList[0]
    for seat in seatList[1:]:
        if(prevSeat + 1 != seat):
            print(f'Your seat is {prevSeat+1}')
        prevSeat = seat




if __name__ == "__main__":
    main()